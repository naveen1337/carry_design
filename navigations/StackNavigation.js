import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import ContainerScreen from '../screens/ContainerScreen';
import HomeScreen from '../screens/HomeScreen';
import RestaurantScreen from '../screens/RestaurantScreen';
import CartScreen from '../screens/CartScreen';
import NoInternetScreen from '../screens/NoInternetScreen';
import UserProfileScreen from '../screens/UserProfileScreen';
import InvoiceScreen from '../screens/invoicescreen';
import PaymentScreen from '../screens/PaymentScreen';
import ProfileEditScreen from '../screens/ProfileEditScreen';
import UserAddressScreen from '../screens/UserAddressScreen';
import FavoritesScreen from '../screens/FavoritesScreen';
import AddressSelection from '../screens/AddressSelection';
import LoginScreen from '../screens/LoginScreen';
import OtpScreen from '../screens/OtpScreen';
import AddressEditScreen from '../screens/AddressEditScreen';
import OnBoardingScreen1 from '../screens/OnBoardingScreen1';
import UserWelcomScreen from '../screens/UserWelcomScreen';
import SearchScreen from '../screens/SearchScreen';
import OfferScreen from '../screens/OfferScreen';
import NoServiceScreen from '../screens/NoserviceScreen';
import OrderTrackingScreen from '../screens/OrderTrackingScreen';
import RestaurantListScreen from '../screens/RestaurantListScreen';
import OrderDetailsScreen from '../screens/OrderDetailsScreen';
import OrderStatusScreen from '../screens/OrderStatusScreen';
import RatingScreen from '../screens/RatingScreen';
import NoGPSScreen from '../screens/NoGPSScreen';

const Stack = createStackNavigator();

export default StackNavigations = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="ContainerScreen" component={ContainerScreen} />
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
      <Stack.Screen name="RestaurantScreen" component={RestaurantScreen} />
      <Stack.Screen name="NoInternetScreen" component={NoInternetScreen} />
      <Stack.Screen name="CartScreen" component={CartScreen} />
      <Stack.Screen name="UserProfileScreen" component={UserProfileScreen} />
      <Stack.Screen name="InvoiceScreen" component={InvoiceScreen} />
      <Stack.Screen name="PaymentScreen" component={PaymentScreen} />
      <Stack.Screen name="ProfileEditScreen" component={ProfileEditScreen} />
      <Stack.Screen name="UserAddressScreen" component={UserAddressScreen} />
      <Stack.Screen name="FavoritesScreen" component={FavoritesScreen} />
      <Stack.Screen
        name="AddressSelectionScreen"
        component={AddressSelection}
      />
      <Stack.Screen name="LoginScreen" component={LoginScreen} />
      <Stack.Screen name="OtpScreen" component={OtpScreen} />
      <Stack.Screen name="AddressEditScreen" component={AddressEditScreen} />
      <Stack.Screen name="OnBoardingScreen1" component={OnBoardingScreen1} />
      <Stack.Screen name="UserWelcomScreen" component={UserWelcomScreen} />
      <Stack.Screen name="SearchScreen" component={SearchScreen} />
      <Stack.Screen name="OfferScreen" component={OfferScreen} />
      <Stack.Screen name="NoServiceScreen" component={NoServiceScreen} />
      <Stack.Screen
        name="OrderTrackingScreen"
        component={OrderTrackingScreen}
      />
      <Stack.Screen
        name="RestaurantListScreen"
        component={RestaurantListScreen}
      />
      <Stack.Screen name="OrderDetailsScreen" component={OrderDetailsScreen} />
      <Stack.Screen name="OrderStatusScreen" component={OrderStatusScreen} />
      <Stack.Screen name="RatingScreen" component={RatingScreen} />
      <Stack.Screen name="NoGPSScreen" component={NoGPSScreen} />
    </Stack.Navigator>
  );
};
