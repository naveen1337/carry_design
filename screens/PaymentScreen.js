import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Pressable,
  Image,
  ScrollView,
  StyleSheet,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import TopBar from '../components/topbar';
import {styles} from '../constants/styles';
import Modal from 'react-native-modal';

import DeliveryRoute from '../components/deliveryroute';

import ButtonComponent from '../components/buttonComponent';

//
export default function InvoiceScreen() {
  const [successmodal, setsuccessmodal] = useState(false);
  return (
    <View style={tailwind('h-full')}>
      <ScrollView>
        <TopBar variant="screenwithicon" title="Checkout" />
        <DeliveryRoute
          from="Hotel Buhari"
          frominfo="1 Item, To Pay `500, ETA 25 MINS"
          toaddress="#120, Purasaiwalkkam Main Road, Doveton"
        />
      </ScrollView>
      <Pressable
        onPress={() => setsuccessmodal(true)}
        style={[tailwind('relative bottom-10 mx-7')]}>
        <ButtonComponent text="CONTINUE" />
      </Pressable>

      {/* Success Modal*/}
      <Modal isVisible={successmodal}>
        <View
          style={[
            tailwind('bg-white py-10 rounded-lg'),
            styles.BackgroundPrimary,
          ]}>
          <View style={tailwind('flex flex-row py-2 justify-center')}>
            <Image
              resizeMode={'contain'}
              style={tailwind('w-36 h-36')}
              source={require('../assets/images/orderdone.png')}
            />
          </View>
          <Text
            style={[
              tailwind('text-3xl text-center text-white'),
              styles.SemiBoldFont,
            ]}>
            Thanks for Buying Food with Us!
          </Text>
          <Text
            style={[
              tailwind('text-xl py-3 text-center text-white'),
              styles.SemiBoldFont,
            ]}>
            your food will arrive in 3 mins
          </Text>
        </View>
        <Pressable
          onPress={() => setsuccessmodal(false)}
          style={[
            tailwind('relative bottom-5 py-4 rounded-lg mx-6 bg-red-700'),
          ]}>
          <Text
            style={[
              tailwind('text-center text-base text-white'),
              styles.SemiBoldFont,
            ]}>
            Track Your Order
          </Text>
        </Pressable>
      </Modal>
    </View>
  );
}
