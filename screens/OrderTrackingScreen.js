import React, {useState} from 'react';
import {
  View,
  Text,
  Pressable,
  Image,
  ScrollView,
  TextInput,
  Dimensions,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {mapStyle} from '../constants/mapstyle';

import MapView, {
  Marker,
  Callout,
  UrlTile,
  PROVIDER_GOOGLE,
} from 'react-native-maps';
import tailwind from 'tailwind-rn';
// import MapViewComponent from '../components/mapview';
import OrderTrackingCard from '../components/orderTrackingCard';
import TopBar from '../components/topbar';
import {styles} from '../constants/styles';

export default function OrderTrackingScreen(props) {
  return (
    <View style={tailwind('h-full')}>
      <TopBar variant="screenwithicon" title="Order Tracking" />
      <View style={[tailwind('')]}>
        <MapView
          style={{height: 200}}
          customMapStyle={mapStyle}
          initialRegion={{
            latitude: 10.3833,
            longitude: 78.8001,
            latitudeDelta: 0,
            longitudeDelta: 0.01,
          }}>
          <Marker
            coordinate={{
              latitude: 10.3833,
              longitude: 78.8001,
              latitudeDelta: 0.0043,
              longitudeDelta: 0.0034,
            }}
            title="Your Current Location"
            description="Sample Text in the card">
            <Image
              resizeMode={'contain'}
              style={tailwind('')}
              source={require('../assets/icons/mapuser.png')}
            />
          </Marker>
        </MapView>
      </View>
      <ScrollView>
        <View style={tailwind('my-4')}>
          <OrderTrackingCard />
        </View>
      </ScrollView>
    </View>
  );
}

// const mystyles = StyleSheet.create({
//   mapheight: {
//     height: Dimensions.get('window').height / 2,
//   },
// });
