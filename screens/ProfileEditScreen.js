import React from 'react';
import {
  View,
  Text,
  Pressable,
  Image,
  ScrollView,
  StyleSheet,
  Dimensions,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import TopBar from '../components/topbar';
import {styles} from '../constants/styles';
import ButtonComponent from '../components/buttonComponent';

export default function InvoiceScreen() {
  return (
    <View style={tailwind('h-full')}>
      <ScrollView>
        <TopBar variant="screen" text="Profile" />
        <View style={tailwind('flex flex-row justify-center')}>
          <Image
            resizeMode={'contain'}
            style={tailwind('w-32 h-32')}
            source={require('../assets/images/person.png')}
          />
        </View>
        <Text
          style={[
            tailwind('text-center text-xs pt-2 pb-1'),
            styles.RegularFont,
            styles.ColorPrimary,
          ]}>
          Edit Profile
        </Text>
        <Text
          style={[
            tailwind('text-center text-lg'),
            styles.SemiBoldFont,
            styles.ColorPrimary,
          ]}>
          Hi there Jaffar!
        </Text>
        <Text
          style={[
            tailwind('text-center text-base py-1'),
            styles.RegularFont,
            styles.ColorPrimary,
          ]}>
          Sign Out
        </Text>
        <View style={tailwind('px-4 my-2')}>
          <TextInput
            placeholder="Name"
            style={[
              tailwind('bg-gray-200 text-base px-4 py-3 rounded-lg'),
              styles.RegularFont,
              styles.ColorGray,
            ]}
          />
        </View>

        <View style={tailwind('px-4 my-2')}>
          <TextInput
            placeholder="Email"
            style={[
              tailwind('bg-gray-200 text-base px-4 py-3 rounded-lg'),
              styles.RegularFont,
              styles.ColorGray,
            ]}
          />
        </View>

        <View style={tailwind('px-4 my-2')}>
          <TextInput
            placeholder="Mobile Number"
            style={[
              tailwind('bg-gray-200 text-base px-4 py-3 rounded-lg'),
              styles.RegularFont,
              styles.ColorGray,
            ]}
          />
        </View>

        <View style={tailwind('px-4 my-2')}>
          <TextInput
            multiline={true}
            placeholder="Address"
            style={[
              tailwind('bg-gray-200 text-base px-4 py-3 rounded-lg'),
              styles.RegularFont,
              styles.ColorGray,
            ]}
          />
        </View>

        <View style={tailwind('px-4 my-2')}>
          <TextInput
            secureTextEntry={true}
            placeholder="Password"
            style={[
              tailwind('bg-gray-200 text-base px-4 py-3 rounded-lg'),
              styles.RegularFont,
              styles.ColorGray,
            ]}
          />
        </View>

        <View style={tailwind('px-4 my-2 pb-20')}>
          <TextInput
            secureTextEntry={true}
            placeholder="Confirm Password"
            style={[
              tailwind('bg-gray-200 text-base px-4 py-3 rounded-lg'),
              styles.RegularFont,
              styles.ColorGray,
            ]}
          />
        </View>
      </ScrollView>
      <Pressable style={[tailwind('relative bottom-2 mx-10')]}>
        <ButtonComponent text="Save" />
      </Pressable>
    </View>
  );
}
