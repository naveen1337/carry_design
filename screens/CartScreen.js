import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Dimensions,
  Pressable,
  Image,
} from 'react-native';
import CartRestaurant from '../components/cartrestaurant';
import CartAddress from '../components/cartaddress';
import CartProduct from '../components/cartproduct';
import CartAction from '../components/cartactions';
import CheckoutButton from '../components/checkoutbutton';
import DeliveryTextInput from '../components/deliveryTextInput';
import InvoiceDetails from '../components/invoicedetails';
import CartInstructions from '../components/cartInstructions';
import Checkoutbutton from '../components/checkoutbutton';
import TopBar from '../components/topbar';

import Tip from '../components/tip';
import NoContactDelivery from '../components/nocontactdelivery';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function CartScreen() {
  const [emptycart, setemptycart] = useState(true);
  return (
    <View style={tailwind('bg-white')}>
      {emptycart ? (
        <View>
          <ScrollView style={[tailwind('py-4')]}>
            <CartRestaurant
              name="Hotel Buhari"
              special="South Indian"
              location="Purasaivakam"
            />
            <CartAddress />
            <CartProduct
              name="Chicken Biriyani"
              hasimage={true}
              quantity="7"
              totalprice="738"
              hasaddon={true}
              hasvariation={true}
              variation="1 pcs"
            />

            <View
              style={tailwind('border border-b-8 my-4 border-gray-100')}></View>

            <CartAction text="Promo Code / Coupon" />

            <DeliveryTextInput />
            <View
              style={tailwind('border border-b-8 my-4 border-gray-100')}></View>

            {/* <Tip /> */}

            <NoContactDelivery />
            <InvoiceDetails />
            <CartInstructions />
            <View style={tailwind('py-10')}></View>
          </ScrollView>
          <View style={tailwind('absolute w-full bottom-0')}>
            <Checkoutbutton amount="748" />
          </View>
        </View>
      ) : (
        <View>
          <TopBar variant="screenwithicon" title="Cart" />
          <View style={tailwind('mt-10')}>
            <View
              style={tailwind(
                'flex flex-col flex-grow justify-center items-center',
              )}>
              <View
                style={tailwind(' flex flex-row justify-center items-center')}>
                <Image
                  resizeMode={'contain'}
                  style={tailwind('w-60 h-60')}
                  source={{
                    uri:
                      'https://i.pinimg.com/originals/b4/56/0c/b4560cf20a1458cf8dc9b739a60a831a.png',
                  }}
                />
              </View>

              <View style={tailwind('w-11/12')}>
                <Text
                  style={[
                    tailwind('text-xl text-gray-600 text-center my-3'),
                    styles.SemiBoldFont,
                  ]}>
                  Oops! Such a Lonely Cart.
                </Text>
                <Text
                  style={[
                    tailwind('text-base text-center text-black'),
                    styles.RegularFont,
                  ]}>
                  Let’s add some carry delights!
                </Text>
              </View>
            </View>
            <View style={[tailwind('flex flex-row justify-center my-10 mx-4')]}>
              <Pressable
                onPress={() => props.navigation.navigate('HomeScreen')}
                style={[
                  tailwind('py-4  w-full rounded rounded-lg'),
                  styles.BackgroundPrimary,
                ]}>
                <Text
                  style={[
                    tailwind('uppercase text-white text-center'),
                    styles.RegularFont,
                  ]}>
                  Discover Goodness
                </Text>
              </Pressable>
            </View>
          </View>
        </View>
      )}
    </View>
  );
}

const mystyles = StyleSheet.create({
  cartbutton: {
    height: '100%',
    backgroundColor: '#56155B',
    paddingVertical: 5,
  },
  opacity: {
    backgroundColor: '#A86398',
  },
});
