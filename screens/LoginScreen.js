import React from 'react';
import {
  View,
  Text,
  Pressable,
  Image,
  ScrollView,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import ButtonComponent from '../components/buttonComponent';
import tailwind from 'tailwind-rn';
import TopBar from '../components/topbar';
import {styles} from '../constants/styles';

export default function LoginScreen(props) {
  let [fav, setfav] = React.useState([1]);
  return (
    <ScrollView>
      <View style={tailwind('flex flex-row items-center p-3')}>
        <Pressable>
          <Icon name="chevron-back-circle" size={24} color="red" />
        </Pressable>
        <View>
          <Text
            style={[
              tailwind('text-lg px-2'),
              styles.ColorPrimary,
              styles.BoldFont,
            ]}>
            Mobile Number Login
          </Text>
          <Text
            style={[
              tailwind('text-sm px-2'),
              styles.ColorGray,
              styles.RegularFont,
            ]}>
            Add your mobile number to Login
          </Text>
        </View>
      </View>

      <View style={tailwind('flex flex-col items-center')}>
        <Image
          resizeMode={'contain'}
          source={require('../assets/images/login.png')}
          style={tailwind('w-60 h-60 mt-10 mb-5')}
        />
        <View
          style={tailwind(
            'flex flex-row items-center border rounded-lg w-80 border-gray-600 rounded',
          )}>
          <Image
            source={require('../assets/icons/dialpad.png')}
            resizeMode={'contain'}
            style={[tailwind('w-6 h-6 mx-4')]}
          />
          <TextInput
            maxLength={14}
            numberOfLines={1}
            keyboardType={'number-pad'}
            placeholder={'Mobile Number'}
            style={[
              styles.RegularFont,
              styles.ColorBlack,
              tailwind('py-3 text-base'),
            ]}
          />
        </View>
        <Pressable
          onPress={() => {
            props.navigation.navigate('OtpScreen');
          }}
          style={tailwind('w-80 my-5')}>
          <ButtonComponent text="SEND OTP" />
        </Pressable>
      </View>
    </ScrollView>
  );
}
