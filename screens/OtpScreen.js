import React from 'react';
import {
  View,
  Text,
  Pressable,
  Image,
  ScrollView,
  TextInput,
} from 'react-native';
import OTPInput from '../components/otpinput';
import Icon from 'react-native-vector-icons/Ionicons';
import ButtonComponent from '../components/buttonComponent';

import tailwind from 'tailwind-rn';
import TopBar from '../components/topbar';
import {styles} from '../constants/styles';

export default function OTPScreen() {
  let [fav, setfav] = React.useState([1]);
  return (
    <ScrollView>
      <View style={tailwind('flex flex-row items-center p-3')}>
        <Pressable>
          <Icon name="chevron-back-circle" size={24} color="red" />
        </Pressable>
        <View>
          <Text
            style={[
              tailwind('text-lg px-2'),
              styles.ColorPrimary,
              styles.BoldFont,
            ]}>
            OTP
          </Text>
        </View>
      </View>

      <View style={tailwind('flex flex-col items-center')}>
        <Image
          resizeMode={'contain'}
          source={require('../assets/images/otp.png')}
          style={tailwind('w-60 h-60 mt-10 mb-5')}
        />
        <Text
          style={[
            tailwind('text-base w-10/12 text-center py-2'),
            styles.RegularFont,
            styles.ColorBlack,
          ]}>
          Please check your mobile +91********15 continue to login
        </Text>
        <View
          style={tailwind(
            'flex flex-row items-center border rounded-lg w-80 border-gray-600 rounded',
          )}>
          <OTPInput />
        </View>
        <Pressable style={tailwind('w-80 my-5')}>
          <ButtonComponent text="Validate" />
        </Pressable>
        <View style={[tailwind('flex flex-row items-center')]}>
          <Text style={[styles.RegularFont, styles.ColorPrimary]}>
            Didn't receive?
          </Text>
          <Text
            style={[
              tailwind('py-2 rounded-lg text-white  px-2 mx-3'),
              styles.BackgroundPrimary,
              styles.RegularFont,
            ]}>
            Click here
          </Text>
        </View>
      </View>
    </ScrollView>
  );
}
