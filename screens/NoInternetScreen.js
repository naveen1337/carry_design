import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import TopBar from '../components/topbar';
import {styles} from '../constants/styles';

export default function NoInternetScreen() {
  return (
    <>
      <TopBar variant="screen" />
      <View style={tailwind('h-full px-1 flex flex-col py-16 items-center')}>
        <Text
          style={[
            tailwind('text-3xl'),
            styles.SemiBoldFont,
            styles.ColorPrimary,
          ]}>
          No Internet Screen
        </Text>
        <Image
          resizeMode={'contain'}
          source={require('../assets/images/nowifi.png')}
          style={tailwind('w-20 h-20')}
        />
        <Text style={[tailwind('text-2xl text-center'), styles.RegularFont]}>
          Your internet connection is currently not available, Please check or
          try again.
        </Text>

        <Pressable
          style={[
            tailwind(
              'absolute bottom-20 my-4 w-5/6 py-4 rounded rounded-xl flex justify-center items-center',
            ),
            styles.BackgroundPrimary,
          ]}>
          <Text
            style={[
              tailwind('text-white uppercase text-lg'),
              styles.SemiBoldFont,
            ]}>
            Try Again
          </Text>
        </Pressable>
      </View>
    </>
  );
}
