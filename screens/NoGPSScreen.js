import React from 'react';
import {View, Text, Image, Pressable, ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import TopBar from '../components/topbar';

export default function NoGPSScreen() {
  return (
    <View style={tailwind('h-full')}>
      <TopBar variant="screen" text="User Permission" />
      <View
        style={tailwind('flex flex-col flex-grow justify-center items-center')}>
        <Text
          style={[
            tailwind('text-lg py-2'),
            styles.SemiBoldFont,
            styles.ColorPrimary,
          ]}>
          NO GPS Enabled
        </Text>
        <Image
          resizeMode={'contain'}
          style={tailwind('w-48 h-48')}
          source={require('../assets/images/deliveryboy.png')}
        />
        <Text
          style={[tailwind('text-center text-base px-3'), styles.RegularFont]}>
          Your GPS Connection is currently not available, Please check or try
          again
        </Text>
      </View>
      <View style={tailwind('absolute bottom-5 w-full')}>
        <Pressable
          elevation={2}
          style={[tailwind('py-3 mx-3 rounded-lg '), styles.BackgroundPrimary]}>
          <Text
            style={[
              tailwind('text-base uppercase text-center text-white'),
              styles.RegularFont,
            ]}>
            Open Setting
          </Text>
        </Pressable>
      </View>
    </View>
  );
}
