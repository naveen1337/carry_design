import React from 'react';
import {View, Text, ScrollView, Pressable} from 'react-native';
import tailwind from 'tailwind-rn';

export default function ContainerScreen(props) {
  return (
    <ScrollView style={tailwind('bg-indigo-100')}>
      <View style={tailwind('mx-6 py-5')}>
        <Pressable
          onPress={() => props.navigation.navigate('NoGPSScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>NoGPSScreen</Text>
        </Pressable>
        <Pressable
          onPress={() => props.navigation.navigate('RatingScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>RatingScreen</Text>
        </Pressable>
        <Pressable
          onPress={() => props.navigation.navigate('OrderStatusScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>
            OrderStatusScreen
          </Text>
        </Pressable>
        <Pressable
          onPress={() => props.navigation.navigate('OrderDetailsScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>
            OrderDetailsScreen
          </Text>
        </Pressable>
        <Pressable
          onPress={() => props.navigation.navigate('RestaurantListScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>
            RestaurantListScreen
          </Text>
        </Pressable>
        <Pressable
          onPress={() => props.navigation.navigate('OrderTrackingScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>
            OrderTrackingScreen
          </Text>
        </Pressable>
        <Pressable
          onPress={() => props.navigation.navigate('NoServiceScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>
            NoServiceScreen
          </Text>
        </Pressable>
        <Pressable
          onPress={() => props.navigation.navigate('SearchScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>Search Screen</Text>
        </Pressable>
        <Pressable
          onPress={() => props.navigation.navigate('OfferScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>Offer Screen</Text>
        </Pressable>

        <Pressable
          onPress={() => props.navigation.navigate('OnBoardingScreen1')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>
            OnBoardingScreen
          </Text>
        </Pressable>
        <Pressable
          onPress={() => props.navigation.navigate('HomeScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>Home Screen</Text>
        </Pressable>
        <Pressable
          onPress={() => props.navigation.navigate('RestaurantScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>
            Restaurant Screen
          </Text>
        </Pressable>
        <Pressable
          onPress={() => props.navigation.navigate('NoInternetScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>
            No Internet Screen
          </Text>
        </Pressable>
        <Pressable
          onPress={() => props.navigation.navigate('CartScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>Cart Screen</Text>
        </Pressable>

        <Pressable
          onPress={() => props.navigation.navigate('UserProfileScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>
            UserProfileScreen
          </Text>
        </Pressable>
        <Pressable
          onPress={() => props.navigation.navigate('UserWelcomScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>
            UserWelcomScreen
          </Text>
        </Pressable>

        <Pressable
          onPress={() => props.navigation.navigate('InvoiceScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>Invoice Screen</Text>
        </Pressable>

        <Pressable
          onPress={() => props.navigation.navigate('PaymentScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>Payment Screen</Text>
        </Pressable>

        <Pressable
          onPress={() => props.navigation.navigate('ProfileEditScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>
            Profile Edit Screen
          </Text>
        </Pressable>

        <Pressable
          onPress={() => props.navigation.navigate('UserAddressScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>
            User Address Screen
          </Text>
        </Pressable>

        <Pressable
          onPress={() => props.navigation.navigate('FavoritesScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>
            Favorites Screen
          </Text>
        </Pressable>

        <Pressable
          onPress={() => props.navigation.navigate('AddressSelectionScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>
            Address Selection Screen
          </Text>
        </Pressable>

        <Pressable
          onPress={() => props.navigation.navigate('LoginScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>Login Screen</Text>
        </Pressable>
        <Pressable
          onPress={() => props.navigation.navigate('AddressEditScreen')}
          style={tailwind(
            'bg-indigo-500 border border-green-300 my-2 rounded px-4 py-3',
          )}>
          <Text style={tailwind('text-white text-center')}>
            Address Edit Screen
          </Text>
        </Pressable>
      </View>
    </ScrollView>
  );
}
