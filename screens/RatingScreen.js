import React from 'react';
import {View, Text, ScrollView, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {AirbnbRating} from 'react-native-ratings';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

import TopBar from '../components/topbar';

export default function RatingScreen() {
  return (
    <ScrollView style={tailwind('')}>
      <TopBar variant="screen" text="Rate Us" />
      <View style={tailwind('flex flex-col justify-between items-center')}>
        <Image
          resizeMode={'contain'}
          style={tailwind('w-52 h-52')}
          source={require('../assets/images/delivery_partner.png')}
        />
        <Text
          style={[tailwind('text-xl text-center py-1'), styles.RegularFont]}>
          How was the delivery ?
        </Text>
        <View style={tailwind('my-4')}>
          <AirbnbRating showRating={false} />
        </View>
        <Text style={[tailwind('text-sm text-center'), styles.RegularFont]}>
          Tap a star to rate
        </Text>
        <Text
          style={[tailwind('text-xl text-center py-1'), styles.RegularFont]}>
          How was the Food ?
        </Text>
        <View style={tailwind('my-4')}>
          <AirbnbRating showRating={false} />
        </View>

        <Text style={[tailwind('text-sm text-center'), styles.RegularFont]}>
          Tap a star to rate
        </Text>
      </View>

      <Pressable
        style={[
          tailwind('py-4 my-3 px-3 mx-3 rounded-lg'),
          styles.BackgroundPrimary,
        ]}>
        <Text
          style={[
            tailwind('text-center text-base text-white uppercase'),
            styles.RegularFont,
          ]}>
          Submit
        </Text>
      </Pressable>
    </ScrollView>
  );
}
