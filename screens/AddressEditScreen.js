import React from 'react';
import {
  View,
  Text,
  Pressable,
  TextInput,
  ScrollView,
  Image,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
// import MapViewComponent from '../components/mapview';
import tailwind from 'tailwind-rn';
import ButtonComponent from '../components/buttonComponent';
import TopBar from '../components/topbar';
import {mapStyle} from '../constants/mapstyle';
import {styles} from '../constants/styles';
import MapView, {
  Marker,
  Callout,
  UrlTile,
  PROVIDER_GOOGLE,
} from 'react-native-maps';

export default function AddressSelection() {
  return (
    <ScrollView style={tailwind('bg-white')}>
      <TopBar variant="screen" text="Choose Address" />
      <View>
        <MapView
          style={{height: 150}}
          customMapStyle={mapStyle}
          initialRegion={{
            latitude: 10.3833,
            longitude: 78.8001,
            latitudeDelta: 0,
            longitudeDelta: 0.01,
          }}>
          <Marker
            coordinate={{
              latitude: 10.3833,
              longitude: 78.8001,
              latitudeDelta: 0.0043,
              longitudeDelta: 0.0034,
            }}
            title="Your Current Location"
            description="Sample Text in the card">
            <Image
              resizeMode={'contain'}
              style={tailwind('')}
              source={require('../assets/icons/mapuser.png')}
            />
          </Marker>
        </MapView>
      </View>

      <View style={tailwind('px-2 py-2')}>
        <Text style={[tailwind('mx-2 text-base py-4'), styles.BoldFont]}>
          Thrivalluvar Nagar
        </Text>
        <TextInput
          placeholder="Address"
          value={
            'No 17320, Kamban Nagar, Rajagopalapuram, Pudukkottai TamilNadu'
          }
          multiline={true}
          style={[
            tailwind('bg-gray-100 mx-2 py-3 text-base rounded-lg'),
            styles.RegularFont,
          ]}
        />
      </View>
      <TextInput
        placeholder="HOUSE/FLAT/BLOCK NO"
        style={[tailwind('bg-gray-100 mx-4 rounded-lg'), styles.RegularFont]}
      />

      <TextInput
        multiline={true}
        placeholder="LANDMARK"
        style={[
          tailwind('bg-gray-100 my-3 mx-4 rounded-lg'),
          styles.RegularFont,
        ]}
      />
      <Text
        style={[
          tailwind('text-sm uppercase mx-4'),
          styles.ColorBlack,
          styles.BoldFont,
        ]}>
        Save as
      </Text>
      <View style={tailwind('flex flex-row justify-between mx-4 my-2')}>
        <Pressable
          style={tailwind(
            'border border-red-500 rounded-lg flex-grow py-2 mx-2',
          )}>
          <Text
            style={[
              tailwind('text-center'),
              styles.RegularFont,
              styles.ColorBlack,
            ]}>
            Home
          </Text>
        </Pressable>
        <Pressable
          style={tailwind(
            'border border-red-500 rounded-lg flex-grow py-2 mx-2',
          )}>
          <Text
            style={[
              tailwind('text-center'),
              styles.RegularFont,
              styles.ColorBlack,
            ]}>
            Work
          </Text>
        </Pressable>
        <Pressable
          style={tailwind(
            'border border-red-500 rounded-lg flex-grow py-2 mx-2',
          )}>
          <Text
            style={[
              tailwind('text-center'),
              styles.RegularFont,
              styles.ColorBlack,
            ]}>
            Other
          </Text>
        </Pressable>
      </View>
      <Pressable style={tailwind('mx-6 my-3')}>
        <ButtonComponent text="SAVE AND UPDATE" />
      </Pressable>
    </ScrollView>
  );
}
