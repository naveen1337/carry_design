import React from 'react';
import {
  View,
  Text,
  Pressable,
  Image,
  ScrollView,
  StyleSheet,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import TopBar from '../components/topbar';
import {styles} from '../constants/styles';
import CheckOutButton from '../components/checkoutbutton';

import InvoiceDetails from '../components/invoicedetails';

export default function InvoiceScreen() {
  return (
    <View>
      <ScrollView style={{height: '92%'}}>
        <View style={tailwind('p-4 flex flex-row items-center')}>
          <Pressable>
            <Icon name="chevron-back-circle" size={30} color="#FF2E2E" />
          </Pressable>

          <View style={tailwind('px-2')}>
            <Text
              style={[
                tailwind('text-base'),
                styles.BoldFont,
                styles.ColorPrimary,
              ]}>
              Hotel Buhari
            </Text>
            <Text
              style={[
                tailwind('text-sm'),
                styles.RegularFont,
                styles.ColorGray,
              ]}>
              1 Item, To Pay 500
            </Text>
          </View>
        </View>
        <InvoiceDetails />
        <Text
          style={[
            tailwind('px-12 mt-4 pb-10'),
            styles.RegularFont,
            styles.ColorPrimary,
          ]}>
          Refund Policy
        </Text>
      </ScrollView>
      <View style={{height: '8%'}}>
        <CheckOutButton />
      </View>
    </View>
  );
}

const mystyles = StyleSheet.create({
  rotate: {
    transform: [{rotate: '180deg'}],
  },
});
