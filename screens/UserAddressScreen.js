import React from 'react';
import {View, Text, ScrollView, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import TopBar from '../components/topbar';
import UserAddress from '../components/useraddress';
import {styles} from '../constants/styles';

export default function FilterScreen() {
  return (
    <ScrollView>
      <View style={tailwind('p-3 flex flex-row justify-between items-center')}>
        <Pressable style={tailwind('flex flex-row items-center')}>
          <Icon name="chevron-back-circle" size={30} color="red" />

          <Text
            style={[
              tailwind('text-base px-3'),
              styles.ColorPrimary,
              styles.BoldFont,
            ]}>
            My Account
          </Text>
        </Pressable>
        <Text
          style={[
            tailwind('uppercase text-sm'),
            styles.ColorPrimary,
            styles.BoldFont,
          ]}>
          Edit
        </Text>
      </View>
      <Text
        style={[
          tailwind('uppercase px-4 text-base'),
          styles.ColorBlack,
          styles.BoldFont,
        ]}>
        Saved Address
      </Text>
      <UserAddress
        type="home"
        address1="Address1"
        address2="City"
        address3="Zip"
      />
      <UserAddress
        type="work"
        address1="Address1"
        address2="City"
        address3="Zip"
      />
      <UserAddress
        type="other"
        address1="Address1"
        address2="City"
        address3="Zip"
      />
    </ScrollView>
  );
}
