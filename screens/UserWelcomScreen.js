import React from 'react';
import {View, Text, Pressable,Image} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import TopBar from '../components/topbar';
import {styles} from '../constants/styles';

export default function UserWelcomScreen(props) {
  return (
    <View style={tailwind('h-full')}>
    <TopBar variant="screenwithicontext" title="Turn on GPS" />
    <View style={tailwind('px-6')}>
      <Text style={[tailwind('text-2xl'),styles.SemiBoldFont,styles.ColorPrimary]}>Hi Jaffar,</Text>
      <Text style={[tailwind('text-2xl'),styles.SemiBoldFont,styles.ColorPrimary]}>Welcome to</Text>
      <Text style={[tailwind('text-2xl'),styles.SemiBoldFont,styles.ColorPrimary]}>Carry</Text>
    </View>
    <View style={tailwind(' flex flex-row justify-center items-center')}>
      <Image resizeMode={'contain'} style={tailwind('w-60 h-60')} source={{uri:"https://i.pinimg.com/originals/bd/41/42/bd4142f0a9db0f9deb86f04c6b3de12e.png"}}/>
    </View>
    <Text style={[tailwind('text-xs text-center text-gray-600'),styles.RegularFont]}>Please turn on your GPS to find out better restaurant suggestions near you.</Text>
    <View style={[tailwind('flex flex-row justify-center my-10 mx-4')]}>
      <Pressable onPress={()=>props.navigation.navigate('HomeScreen')} style={[tailwind('py-4  w-full rounded rounded-lg'),styles.BackgroundPrimary]}>
        <Text style={[tailwind('uppercase text-white text-center'),styles.RegularFont]}>turn on gps</Text>
      </Pressable>

    </View>
    </View>
  );
}
