import React from 'react';
import {
  View,
  Text,
  Pressable,
  Image,
  ScrollView,
  StyleSheet,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import TopBar from '../components/topbar';
import {styles} from '../constants/styles';
import OrderDetails from '../components/orderDetails';

export default function InvoiceScreen() {
  return (
    <View style={tailwind('h-full')}>
      <ScrollView>
        <TopBar variant="screenwithicon" title="Order Details" />
        <OrderDetails
          orderid="CARKF22022131"
          price="383"
          item="Hot & Crispy Chicken"
          time="22-02-2021 11.04.AM"
          deliveryrating="4"
          foodrating="3"
          completed={false}
        />
         <OrderDetails
          orderid="CARKF22022131"
          price="383"
          item="Hot & Crispy Chicken"
          time="22-02-2021 11.04.AM"
          deliveryrating="4"
          foodrating="3"
          completed={true}
        />
      </ScrollView>
    </View>
  );
}
