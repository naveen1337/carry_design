import React from 'react';
import {View, ScrollView, Text, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import SpotLightRestaurant from '../components/spotlight_restaurant';
import FilterDialog from '../components/filterdialog';
import SortandFilter from '../components/sortandfilter';
import TopBar from '../components/topbar';

export default function RestaurantListScreen() {
  let [modal, setmodal] = React.useState(false);
  function closemodal() {
    setmodal(!modal);
  }
  return (
    <ScrollView style={tailwind('bg-white')}>
      <TopBar variant="screenwithicon" title="" />
      <View style={tailwind('flex flex-row justify-between')}>
        <Text></Text>
        <Pressable
          onPress={() => setmodal(!modal)}
          style={tailwind('flex flex-row items-center')}>
          <Image
            resizeMode={'contain'}
            style={tailwind('w-5 h-5')}
            source={require('../assets/images/pick.png')}
          />
          <Text
            style={[
              tailwind('uppercase text-sm px-3'),
              styles.RegularFont,
              styles.ColorGray,
            ]}>
            Sort / Filter
          </Text>
        </Pressable>
      </View>
      <Text style={[tailwind('px-6 py-3 text-base'), styles.SemiBoldFont]}>
        Nearby Restaurants
      </Text>
      <SpotLightRestaurant
        name="Hotel Buhari"
        special="Arabian, Biryani, Chinese, Juices"
        location="Purasaiwakkam, 2.2 kms"
        rating="4.5"
        distance="20 mins"
        price="Rs 600 for two"
        offer="offer 34%"
      />
      <FilterDialog status={modal} closemodal={closemodal} />
      <View style={tailwind('mb-20')}></View>
    </ScrollView>
  );
}
