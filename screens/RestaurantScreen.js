import React, {useState, useEffect} from 'react';
import {View, Text, ScrollView, Image, Pressable, Alert} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import TopBar from '../components/topbar';
import RestaurantCard from '../components/restaurantcard';
import RestaurantOffers from '../components/restaurantoffers';
import CategoryTitle from '../components/categoryTitle';
import ToggleSwitch from 'toggle-switch-react-native';

import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import Modal from 'react-native-modal';

export default function RestaurantScreen() {
  const [addonmodal, setaddonmodal] = useState(false);
  const [clearcartdialog, setclearcartdialog] = useState(false);
  const [categorymodal, setcategorymodal] = useState(false);
  const [veg, setveg] = useState(false);
  return (
    <View style={tailwind('h-full bg-white')}>
      <ScrollView style={tailwind('bg-white')}>
        <TopBar varient="restaurant" />
        <RestaurantCard
          brandname="Hotel Buhari"
          image="https://i.pinimg.com//originals//78//89//9b//78899b80c37e5ed8d16de6d65a5c9811.png"
          status="open"
          location="Purasaivaikam"
          rating="4.1"
          special="South Indian"
          deliverytime="20 min"
          price="400"
        />
        <ScrollView
          style={tailwind('py-1 mr-4')}
          horizontal={true}
          showsHorizontalScrollIndicator={false}>
          <RestaurantOffers
            offer="50% OFF"
            offercode="USE CARRY | 100% Cashback"
          />
          <RestaurantOffers
            offer="70% OFF"
            offercode="USE TEST | 80% Cashback"
          />
          <RestaurantOffers
            offer="50% OFF + `50 CASHBA.."
            offercode="USE CARRY | 100% Cashback"
          />
        </ScrollView>
        <Text style={tailwind('border-b-2 border-gray-500')}></Text>

        <View style={tailwind('flex flex-row py-2 px-4')}>
          <ToggleSwitch
            isOn={veg}
            onColor="white"
            offColor="white"
            size="small"
            thumbOnStyle={{backgroundColor: 'red'}}
            thumbOffStyle={{backgroundColor: 'green'}}
            trackOnStyle={{borderWidth: 1, borderColor: '#b2bec3'}}
            trackOffStyle={{borderWidth: 1, borderColor: '#b2bec3'}}
            animationSpeed={100}
            onToggle={() => setveg(!veg)}
          />
          {veg ? (
            <Text style={[tailwind('px-2 uppercase'), styles.SemiBoldFont]}>
              Veg / Non Veg
            </Text>
          ) : (
            <Text style={[tailwind('px-2 uppercase'), styles.SemiBoldFont]}>
              Veg only
            </Text>
          )}
        </View>

        {/*Load Category Titles*/}
        <CategoryTitle title="Category One" />

        <View style={tailwind('mb-20 bg-green-300')}></View>
      </ScrollView>

      {/* Floating Button on Browse Menu  */}
      <View style={tailwind(' flex flex-row justify-center')}>
        <Pressable
          onPress={() => {
            setcategorymodal(true);
          }}
          style={[
            tailwind(
              'absolute bottom-2 bg-blue-200 flex rounded py-1 rounded-lg px-2 flex-row items-center',
            ),
            styles.BackgroundPurpleDark,
          ]}>
          <Image
            resizeMode={'contain'}
            source={require('../assets/images/pick2.png')}
            style={tailwind('w-8 h-8')}
          />
          <Text style={[styles.SemiBoldFont, tailwind('uppercase text-white')]}>
            Browser Menu
          </Text>
        </Pressable>
      </View>

      {/* Category  Modal */}
      <Modal
        isVisible={categorymodal}
        style={{justifyContent: 'flex-end', margin: 0}}>
        <View style={tailwind('bg-white px-3 py-2 h-80')}>
          <View style={tailwind('flex flex-row justify-between items-center')}>
            <Text style={[tailwind('text-lg pt-2 pb-4'), styles.SemiBoldFont]}>
              Browser Menu
            </Text>
            <Pressable onPress={() => setcategorymodal(false)}>
              <Icon name="close" size={25} color="#000000" />
            </Pressable>
          </View>
          <ScrollView style={tailwind('')}>
            <Pressable
              style={tailwind('flex flex-row justify-between px-4 py-2 my-2')}>
              <Text
                style={[tailwind('text-base uppercase'), styles.SemiBoldFont]}>
                Category 1
              </Text>
              <Text style={[tailwind('text-base'), styles.SemiBoldFont]}>
                3
              </Text>
            </Pressable>
            <Pressable
              style={tailwind('flex flex-row justify-between px-4 py-2 my-2')}>
              <Text
                style={[tailwind('text-base uppercase'), styles.SemiBoldFont]}>
                Category 2
              </Text>
              <Text style={[tailwind('text-base'), styles.SemiBoldFont]}>
                5
              </Text>
            </Pressable>
          </ScrollView>
        </View>
      </Modal>

      {/* Clear Cart Dialog */}
      <Modal isVisible={clearcartdialog}>
        <View style={tailwind('bg-white pt-2')}>
          <View style={tailwind('px-2 py-5')}>
            <Text style={[tailwind('text-base mx-3'), styles.SemiBoldFont]}>
              Do you want to add the other restaurant menu in your Cart ?
            </Text>
          </View>
          <View style={tailwind('flex flex-row justify-between')}>
            <Pressable
              onPress={() => setclearcartdialog(false)}
              style={[
                tailwind('flex-grow py-3'),
                styles.BackgroundPurpleLight,
              ]}>
              <Text
                style={[
                  tailwind('text-center text-base text-white'),
                  styles.RegularFont,
                ]}>
                Cancel
              </Text>
            </Pressable>

            <Pressable
              onPress={() => setclearcartdialog(false)}
              style={[tailwind('flex-grow py-3'), styles.BackgroundPurpleDark]}>
              <Text
                style={[
                  tailwind('text-center text-base text-white'),
                  styles.RegularFont,
                ]}>
                Proceed
              </Text>
            </Pressable>
          </View>
        </View>
      </Modal>
    </View>
  );
}
