import React from 'react';
import {View, Text, Image, Pressable, ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import Collapsible from 'react-native-collapsible';

import UserTopBar from '../components/usertopbar';
import PastOrder from '../components/pastorder';
import UserProfileInfo from '../components/accordian';

export default function UserProfileScreen(props) {
  return (
    <ScrollView style={tailwind('px-2')}>
      <UserTopBar />
      <View
        style={tailwind(
          'mx-2 border-b-2 border-gray-400 pt-1 pb-3  flex flex-row items-center',
        )}>
        <Image
          resizeMode={'contain'}
          style={tailwind('w-16 h-16 rounded-full')}
          source={require('../assets/images/splash_logo.png')}
        />
        <View style={tailwind('px-2')}>
          <Text style={[tailwind('text-lg'), styles.BoldFont]}>Naveen</Text>

          <Text style={[tailwind('text-gray-600'), styles.RegularFont]}>
            +91987654321
          </Text>
          <Text style={[tailwind('text-gray-600'), styles.RegularFont]}>
            mail@example.com
          </Text>
        </View>
      </View>

      <UserProfileInfo header="My Account" variant="account" />
      <UserProfileInfo header="Payments & Refunds" variant="payment" />
      <UserProfileInfo header="Help, Faq’s & Links" variant="faq" />

      <Pressable style={tailwind('my-3 bg-gray-200 rounded px-4 py-3')}>
        <Text
          style={[
            tailwind('text-base uppercase'),
            styles.SemiBoldFont,
            styles.ColorBlack,
          ]}>
          Past orders
        </Text>
      </Pressable>
      <PastOrder
        name="Hotel Buhari"
        status="delivered"
        location="Mogappair"
        amount="500"
        date="December 25, 12:25 PM"
        items={['Biryani (1 Qty) x 1']}
      />

      <Pressable
        style={tailwind(
          'my-3 px-6 py-3 bg-gray-200 rounded flex flex-row justify-between',
        )}>
        <Text
          style={[
            tailwind('text-base uppercase'),
            styles.SemiBoldFont,
            styles.ColorBlack,
          ]}>
          Log Out
        </Text>
        <Icon name="power-outline" size={20} color="#FF2E2E" />
      </Pressable>
      <Text
        style={[
          tailwind('my-6 text-center text-xs'),
          styles.ColorGray,
          styles.RegularFont,
        ]}>
        App Version 1.0
      </Text>
    </ScrollView>
  );
}
