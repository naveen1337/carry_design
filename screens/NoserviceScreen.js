import React from 'react';
import {
  View,
  Text,
  Pressable,
  Image,
  ScrollView,
  TextInput,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import ButtonComponent from '../components/buttonComponent';
import tailwind from 'tailwind-rn';
import AddressTopBar from '../components/addresstopbar';
import {styles} from '../constants/styles';

export default function NoServiceScreen(props) {
  return (
    <View>
      <ScrollView>
        <AddressTopBar address="#120, Purasaiwalkkam Main Road, Doveton" />

        <View
          style={[
            tailwind('flex flex-col justify-center items-center'),
            {marginTop: Dimensions.get('window').height / 10},
          ]}>
          <Image
            resizeMode={'contain'}
            style={tailwind('w-80 h-80')}
            source={require('../assets/images/noservice.png')}
          />
          <Text
            style={[
              tailwind('text-lg text-gray-700 py-2 px-3'),
              styles.SemiBoldFont,
            ]}>
            We're not here yet
          </Text>
          <Text
            style={[
              tailwind('text-center text-gray-700 px-3'),
              styles.RegularFont,
            ]}>
            We would to serve but unfortuanately our service are not available
            at this location
          </Text>
        </View>
      </ScrollView>
    </View>
  );
}
