import React from 'react';
import {View, Text, Pressable, Image} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import FavoriteRestaurant from '../components/favoriterestaurant';
import tailwind from 'tailwind-rn';
import TopBar from '../components/topbar';
import {styles} from '../constants/styles';

export default function FilterScreen() {
  let [fav, setfav] = React.useState([1]);
  return (
    <View>
      <TopBar variant="screen" text="Favorites" />
      {fav.length <= 0 ? (
        <View style={tailwind('flex flex-col justify-center items-center')}>
          <Image
            resizeMode={'contain'}
            style={tailwind('w-60 h-60 rounded rounded-lg')}
            source={require('../assets/images/emptyfavorites.png')}
          />
          <Text
            style={[
              tailwind('text-xl mb-2'),
              styles.SemiBoldFont,
              styles.ColorPrimary,
            ]}>
            Favourites Heading?
          </Text>
          <Text style={[tailwind('text-center mx-4'), styles.RegularFont]}>
            Once you favourite a restaurant, it will appear here.
          </Text>
        </View>
      ) : (
        <>
          <FavoriteRestaurant
            name="Hotel Buhari"
            special="Arabian, Biryani, Chinese, Juices"
            location="Purasaiwakkam, 2.2 kms"
            rating="4.5"
            distance="20 mins"
            price="Rs 600 for two"
            offer="offer 34%"
          />
          <FavoriteRestaurant
            name="Hotel Buhari"
            special="Arabian, Biryani, Chinese, Juices"
            location="Purasaiwakkam, 2.2 kms"
            rating="4.5"
            distance="20 mins"
            price="Rs 600 for two"
            offer="offer 34%"
          />
        </>
      )}
    </View>
  );
}
