import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import TopBar from '../components/topbar';
import OrderStatusCard from '../components/orderStatusCard';
import OrderProduct from '../components/orderProduct';
import PricingCard from '../components/pricingCard';
import {styles} from '../constants/styles';

export default function OrderStatusScreen() {
  return (
    <View>
      <ScrollView style={tailwind('bg-white')}>
        <TopBar variant="screenwithicon" title="Order Status" />
        <OrderStatusCard
          id="CARKF22022132"
          time="22-02-2021 5:36 PM"
          status={1}
        />
        <View style={tailwind('flex flex-row mx-4 pt-4 pb-2 items-center')}>
          <Text style={[tailwind('text-base'), styles.SemiBoldFont]}>
            Order at
          </Text>
          <Text
            style={[
              tailwind('text-base px-3'),
              styles.SemiBoldFont,
              styles.ColorPrimary,
            ]}>
            KFC
          </Text>
        </View>

        <OrderProduct
          name="Hot & Crispy Chicken"
          desc="2 pc Hot & Crispy Chicken"
          quantity="3"
          price="360"
        />
        <OrderProduct
          name="Hot & Crispy Chicken"
          desc="2 pc Hot & Crispy Chicken"
          quantity="2"
          price="370"
        />
        <Text
          style={[tailwind('mx-4 text-sm text-gray-500'), styles.RegularFont]}>
          Delivery Address
        </Text>

        <View
          style={tailwind(' mx-4 rounded-xl my-3 border p-3 border-gray-300')}>
          <Text
            style={[
              tailwind('px-4 text-sm text-gray-500'),
              styles.SemiBoldFont,
            ]}>
            Test 2329 MKK Nagar Thiruvalluvar Nagar Pudukkottai Tamil Nadu
            622303 India
          </Text>
        </View>
        <Text
          style={[
            tailwind('mx-4 text-sm text-gray-500 py-2'),
            styles.RegularFont,
          ]}>
          Amount
        </Text>
        <PricingCard total={370} delivery={2832} packing={23} tax={16} />

        <View style={tailwind('flex flex-row justify-between py-2')}>
          <Text
            style={[
              tailwind('mx-6 text-sm text-gray-500 py-2'),
              styles.RegularFont,
            ]}>
            Mode of Payment
          </Text>
          <Text
            style={[
              tailwind('mx-6 text-sm text-gray-500 py-2'),
              styles.RegularFont,
            ]}>
            Cash on Delivery
          </Text>
        </View>
        <View style={tailwind('pb-20')}></View>
      </ScrollView>
      <View style={tailwind('absolute  w-full bottom-5')}>
        <Pressable
          style={[
            tailwind('rounded-lg py-4 px-3 mx-4 flex flex-row items-center '),
            styles.BackgroundPrimary,
          ]}>
          <Text style={[tailwind('text-xs text-white'), styles.RegularFont]}>
            Total
          </Text>
          <Text
            style={[
              tailwind('text-base text-white px-3'),
              styles.SemiBoldFont,
            ]}>
            ₹ 3327
          </Text>
        </Pressable>
      </View>
    </View>
  );
}
