import * as React from 'react';
import { View, StyleSheet, Dimensions,Text,Pressable} from 'react-native';
import { TabView, SceneMap,TabBar} from 'react-native-tab-view';
import TopBar from '../components/topbar';
import tailwind from 'tailwind-rn';

const FirstRoute = () => (
  <View style={[styles.scene, { backgroundColor: '#ff4081' }]} />
);

const SecondRoute = () => (
  <View style={[styles.scene, { backgroundColor: '#673ab7' }]} />
);

const initialLayout = { width: Dimensions.get('window').width };

const renderTabBar = (props) => (
  <TabBar
    renderLabel={({route, focused, color}) =>
      focused ? (
        <Text
          style={[tailwind('text-base')]}>
          {route.title}
        </Text>
      ) : (
        <Text
          style={{
            color: '#FFEAC0',
            fontSize: 16,
          }}>
          {route.title}
        </Text>
      )
    }
    {...props}
    indicatorStyle={{backgroundColor: 'white'}}
    style={{backgroundColor: 'white'}}
  />
);

export default function TabViewExample() {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'first', title: 'Restaurnat Offers' },
    { key: 'second', title: 'My Coupons' },
  ]);

  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
  });

  return (
    <>
    <TopBar variant="screen" text="Offers" />
    <TabView
      navigationState={{ index, routes }}
      renderTabBar={renderTabBar}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={initialLayout}
    />
  </>
  );
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
});
