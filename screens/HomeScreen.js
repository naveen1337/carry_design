import React from 'react';
import {View, Text, FlatList, ScrollView, Pressable} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

import AddressTopBar from '../components/addresstopbar';
import HomeImageSlider from '../components/homeimageslider';
import StaticBanner from '../components/staticbanners';
import TopBar from '../components/topbar';
import TopRestaurant from '../components/toprestaurant';
import SpotLightTitle from '../components/spotlighttitle';
import SortandFilter from '../components/sortandfilter';
import SpotLightRestaurant from '../components/spotlight_restaurant';
import NotificationScroll from '../components/notificationscroll';
import HomeCoupons from '../components/homecoupons';
import DealoftheDay from '../components/dealoftheday';
import HomeFooter from '../components/homefooter';
import FilterDialog from '../components/filterdialog';


export default function HomeScreen() {  
  let [modal, setmodal] = React.useState(false);
  let [loading, setloading] = React.useState(false);
  
  React.useEffect(() => {
    setTimeout(() => {
      setloading(true);
    }, 500);
  }, []);

  function closemodal() {
    setmodal(!modal);
  }
  return (
    <ScrollView style={tailwind('bg-white')}>
      <AddressTopBar address="#120, Purasaiwalkkam Main Road, Doveton" />
      <HomeImageSlider />

      <StaticBanner />
      <TopBar variant="screenwithicon" title="Top Restaurants For You" />

      <ScrollView
        style={tailwind('mx-4')}
        horizontal={true}
        showsHorizontalScrollIndicator={false}>
        <TopRestaurant
          hasoffer={true}
          name="Hotel Buhari"
          offer="50% Off"
          time="21 mins"
        />
        <TopRestaurant
          hasoffer={true}
          name="Chai’s N Chills"
          offer="34% Off"
          time="26 mins"
        />
        <TopRestaurant
          name="Madras Chill Jigarthanda"
          hasoffer={true}
          offer="38% Off"
          time="36 mins"
        />
        <TopRestaurant
          hasoffer={true}
          name="Great Wall of China"
          offer="58% Off"
          time="25 mins"
        />
        <TopRestaurant
          name="Hotel Buhari"
          hasoffer={true}
          offer="63% Off"
          time="64 mins"
        />
      </ScrollView>

      <HomeImageSlider />

      {loading ? (
        <>
          <SpotLightTitle
            title="In the Spotlight"
            subtitle="Explore the sponsored partner brands"
          />
          <Pressable onPress={() => setmodal(!modal)}>
            <SortandFilter />
          </Pressable>
          <SpotLightRestaurant
            name="Hotel Buhari"
            special="Arabian, Biryani, Chinese, Juices"
            location="Purasaiwakkam, 2.2 kms"
            rating="4.5"
            distance="20 mins"
            price="Rs 600 for two"
            offer="offer 34%"
          />
          <SpotLightRestaurant
            name="Hotel Buhari"
            special="Arabian, Biryani, Chinese, Juices"
            location="Purasaiwakkam, 2.2 kms"
            rating="4.5"
            distance="20 mins"
            price="Rs 600 for two"
            offer="offer 34%"
          />
          <ScrollView
            style={tailwind('mx-3')}
            horizontal={true}
            showsHorizontalScrollIndicator={false}>
            <NotificationScroll text="100+Biryanis ordered in your neighbourhood in the last few hours" />
            <NotificationScroll text="100+Biryanis ordered in your neighbourhood in the last few hours" />
          </ScrollView>
          <SpotLightTitle
            foodicon={false}
            title="Top Coupon For You"
            subtitle={false}
          />
          <ScrollView
            style={tailwind('mx-3')}
            horizontal={true}
            showsHorizontalScrollIndicator={false}>
            <HomeCoupons image={1} />
            <HomeCoupons image={2} />
            <HomeCoupons image={3} />
          </ScrollView>
          <TopBar
            variant="screenwithicon"
            title="Popular Cuisines Around You"
          />

          <ScrollView
            style={tailwind('mx-4')}
            horizontal={true}
            showsHorizontalScrollIndicator={false}>
            <TopRestaurant
              hasoffer={false}
              name="Hotel Buhari"
              time="21 mins"
            />
            <TopRestaurant
              hasoffer={false}
              name="Chai’s N Chills"
              time="26 mins"
            />
            <TopRestaurant
              hasoffer={false}
              name="Madras Chill Jigarthanda"
              time="36 mins"
            />
            <TopRestaurant
              hasoffer={false}
              name="Great Wall of China"
              time="25 mins"
            />
            <TopRestaurant
              hasoffer={false}
              name="Hotel Buhari"
              time="64 mins"
            />
          </ScrollView>
          <TopBar variant="screenwithicon" title="Popular Brands Around You" />

          <ScrollView
            style={tailwind('mx-4')}
            horizontal={true}
            showsHorizontalScrollIndicator={false}>
            <TopRestaurant
              hasoffer={false}
              name="Hotel Buhari"
              time="21 mins"
            />
            <TopRestaurant
              hasoffer={false}
              name="Chai’s N Chills"
              time="26 mins"
            />
            <TopRestaurant
              hasoffer={false}
              name="Madras Chill Jigarthanda"
              time="36 mins"
            />
            <TopRestaurant
              hasoffer={false}
              name="Great Wall of China"
              time="25 mins"
            />
            <TopRestaurant
              hasoffer={false}
              name="Hotel Buhari"
              time="64 mins"
            />
          </ScrollView>
          <SpotLightTitle
            title="Deal of the Day"
            subtitle="Get your favourite restaurants at a deal"
          />
          <ScrollView
            style={tailwind('mx-4')}
            horizontal={true}
            showsHorizontalScrollIndicator={false}>
            <DealoftheDay
              title="Breakfast Special"
              subtitle="North Indian, South Indian"
            />
            <DealoftheDay
              title="Breakfast Special"
              subtitle="North Indian, South Indian"
            />
            <DealoftheDay
              title="Breakfast Special"
              subtitle="North Indian, South Indian"
            />
          </ScrollView>
          <SpotLightTitle
            foodicon={true}
            title="Popular Brands Around You"
            subtitle={false}
          />

          <SpotLightRestaurant
            name="Hotel Buhari"
            special="Arabian, Biryani, Chinese, Juices"
            location="Purasaiwakkam, 2.2 kms"
            rating="4.5"
            distance="20 mins"
            price="Rs 600 for two"
            offer="offer 34%"
          />
        </>
      ) : (
        <></>
      )}

      <HomeFooter />

      <FilterDialog status={modal} closemodal={closemodal} />

      <View style={tailwind('mb-20')}></View>
    </ScrollView>
  );
}
