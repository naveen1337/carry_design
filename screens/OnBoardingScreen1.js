import React, {useState} from 'react';
import {
  View,
  Text,
  Pressable,
  ScrollView,
  Image,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import TopBar from '../components/topbar';
import {styles} from '../constants/styles';
import Swiper from 'react-native-swiper';

export default function OnBoardingScreen1(props) {
  const [index, setIndex] = useState(0);
  const [swipe, setswipe] = useState(0);

  const indexswiped = (e) => {
    setTimeout(() => {
      setswipe(e);
      setIndex(e);
    }, 100);
  };

  const handleClick = () => {
    if (index <= 1) {
      console.log('index', index);
      setIndex(index + 1);
    }
  };

  return (
    <View style={tailwind('bg-white flex flex-col py-5 h-full')}>
      <View style={tailwind('flex flex-col justify-center items-center')}>
        <Image
          resizeMode={'contain'}
          style={tailwind('w-60 h-32')}
          source={require('../assets/images/logo.png')}
        />
      </View>
      <Swiper
        index={index}
        style={tailwind('h-80')}
        onIndexChanged={(e) => indexswiped(e)}
        loop={false}
        showsButtons={false}
        dot={
          <View
            style={{
              backgroundColor: 'rgba(0,0,0,.2)',
              width: 15,
              height: 4,
              borderRadius: 0,
              margin: 4,
            }}
          />
        }
        activeDot={
          <View
            style={{
              backgroundColor: '#FF2E2E',
              width: 15,
              height: 4,
              borderRadius: 0,
              margin: 4,
            }}
          />
        }>
        <View style={tailwind('flex flex-row justify-center items-center')}>
          <View style={tailwind('flex flex-col justify-center items-center')}>
            <Image
              resizeMode={'contain'}
              style={tailwind('w-32 h-32')}
              source={{
                uri:
                  'https://i.pinimg.com/originals/b4/56/0c/b4560cf20a1458cf8dc9b739a60a831a.png',
              }}
            />
            <Text
              style={[tailwind('py-2 text-center text-2xl'), styles.BoldFont]}>
              Fresh Food & Everything
            </Text>
            <Text
              style={[
                tailwind('py-2 text-center text-base'),
                styles.RegularFont,
              ]}>
              Discover the best foods from over a million restaurants and fast
              deliver to your doorstep.
            </Text>
          </View>
        </View>
        <View style={tailwind('flex flex-row justify-center items-center')}>
          <View style={tailwind('flex flex-col justify-center items-center')}>
            <Image
              resizeMode={'contain'}
              style={tailwind('w-32 h-32')}
              source={{
                uri:
                  'https://i.pinimg.com/originals/bd/41/42/bd4142f0a9db0f9deb86f04c6b3de12e.png',
              }}
            />
            <Text
              style={[tailwind('py-2 text-center text-2xl'), styles.BoldFont]}>
              Fast Delivery
            </Text>
            <Text
              style={[
                tailwind('py-2 mx-2 text-center text-base'),
                styles.RegularFont,
              ]}>
              Fast delivery to your home, office and wherever you are
            </Text>
          </View>
        </View>

        <View style={tailwind('flex flex-row justify-center items-center')}>
          <View style={tailwind('flex flex-col justify-center items-center')}>
            <Image
              resizeMode={'contain'}
              style={tailwind('w-32 h-32')}
              source={{
                uri:
                  'https://i.pinimg.com/originals/03/bf/e0/03bfe0e700b312938ea4d76b47637c47.png',
              }}
            />
            <Text
              style={[
                tailwind('py-2 mx-2 text-center text-2xl'),
                styles.BoldFont,
              ]}>
              Enjoy Today
            </Text>
            <Text
              style={[
                tailwind('py-2 mx-2 text-center text-base'),
                styles.RegularFont,
              ]}>
              Real time tracking of your food on the app once you placed the
              order
            </Text>
          </View>
        </View>
      </Swiper>

      <View style={[tailwind('flex flex-row justify-between mx-4 mb-10')]}>
        {!(index == 2 || swipe == 2) ? (
          <Pressable
            onPress={() => props.navigation.navigate('HomeScreen')}
            style={[
              tailwind('py-2 px-7 rounded rounded-lg'),
              styles.BackgroundPrimary,
            ]}>
            <Text
              style={[tailwind('uppercase text-white'), styles.RegularFont]}>
              Skip
            </Text>
          </Pressable>
        ) : null}

        {index == 2 || swipe == 2 ? (
          <Pressable
            onPress={() => props.navigation.navigate('HomeScreen')}
            style={[
              tailwind('py-4 w-full px-7 rounded rounded-lg'),
              styles.BackgroundPrimary,
            ]}>
            <Text
              style={[
                tailwind('uppercase text-base text-white text-center'),
                styles.RegularFont,
              ]}>
              Get started
            </Text>
          </Pressable>
        ) : (
          <Pressable
            onPress={handleClick}
            style={[
              tailwind('py-2 px-7 rounded rounded-lg'),
              styles.BackgroundPrimary,
            ]}>
            <Text
              style={[tailwind('uppercase text-white'), styles.RegularFont]}>
              Next
            </Text>
          </Pressable>
        )}
      </View>
    </View>
  );
}
