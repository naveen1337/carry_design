import React, {useState} from 'react';
import {
  View,
  Text,
  Pressable,
  TextInput,
  ScrollView,
  Image,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import TopBar from '../components/topbar';
import {styles} from '../constants/styles';
import {mapStyle} from '../constants/mapstyle';

import MapView, {
  Marker,
  Callout,
  UrlTile,
  PROVIDER_GOOGLE,
} from 'react-native-maps';

export default function AddressSelection() {
  const [locallocation, setlocallocation] = useState({
    latitude: 10.3833,
    longitude: 78.8001,
  });
  return (
    <ScrollView>
      <TopBar variant="screen" text="Choose Address" />
      <View></View>

      <View style={[tailwind('')]}>
        <MapView
          onRegionChangeComplete={(e) => {
            console.log(e);
            setlocallocation({
              latitude: e.latitude,
              longitude: e.longitude,
            });
          }}
          style={{height: 200}}
          showsUserLocation={true}
          showsMyLocationButton={true}
          zoomControlEnabled={true}
          loadingEnabled={true}
          customMapStyle={mapStyle}
          initialRegion={{
            latitude: 10.3833,
            longitude: 78.8001,
            latitudeDelta: 0,
            longitudeDelta: 0.01,
          }}>
          <Marker
            coordinate={{
              latitude: locallocation.latitude,
              longitude: locallocation.longitude,
              latitudeDelta: 0.0043,
              longitudeDelta: 0.0034,
            }}
            // Need to be shrink
            icon={require('../assets/icons/pin_target.png')}></Marker>
        </MapView>
      </View>

      <View>
        <View
          elevation={1}
          style={tailwind(
            'border mx-4 mt-3 py-0 px-3 border-red-500 rounded flex flex-row items-center ',
          )}>
          <Icon name="search" size={25} color="#636e72" />
          <TextInput
            placeholder="Search Address"
            editable={false}
            style={[
              tailwind('flex-grow text-base text-gray-800'),
              styles.SemiBoldFont,
            ]}
          />
          <Icon name="close" size={20} color="#636e72" />
          <Icon
            style={tailwind('px-3')}
            name="chevron-forward-outline"
            size={20}
            color="#636e72"
          />
        </View>

        {/* Address Section */}

        <View style={tailwind('mx-3')}>
          <TextInput
            placeholder="Address Title"
            editable={false}
            value="The location"
            style={[tailwind('text-lg text-gray-800'), styles.SemiBoldFont]}
          />
          <TextInput
            placeholder="Address"
            multiline={true}
            editable={false}
            value="221. M.K.K Nagar, Kamban Nagar, Thiruvalluvar Nagar, Pudukkottai, Tamil Nadu 622303"
            style={[tailwind('text-gray-800'), styles.RegularFont]}
          />
          <Pressable
            style={[
              tailwind('py-3 my-2 rounded-lg'),
              styles.BackgroundPrimary,
            ]}>
            <Text
              style={[
                tailwind('text-white text-base text-center'),
                styles.RegularFont,
              ]}>
              Confirm Location
            </Text>
          </Pressable>
        </View>

        <View
          style={tailwind(
            'mx-4 mt-2 flex flex-row items-center justify-between',
          )}>
          <View style={tailwind('flex flex-row items-center')}>
            <View
              style={[tailwind('rounded-full p-1'), styles.BackgroundPrimary]}>
              <Icon name="star" size={20} color="white" />
            </View>
            <Text
              style={[
                tailwind('text-base px-3'),
                styles.SemiBoldFont,
                styles.ColorPrimary,
              ]}>
              Choose a saved place
            </Text>
          </View>
          <Icon
            name="chevron-forward"
            style={tailwind('text-center')}
            size={25}
            color="#FF2E2E"
          />
        </View>
      </View>
    </ScrollView>
  );
}
