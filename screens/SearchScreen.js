import React,{useState}from 'react';
import {
  View,
  Text,
  Pressable,
  Image,
  ScrollView,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import TopBar from '../components/topbar';
import {styles} from '../constants/styles';
import SpotLightRestaurant from '../components/spotlight_restaurant';

export default function SearchScreen(props) {
  const [empty,setempty] = useState(false)
  return (
    <View style={tailwind('h-full bg-white')}>
      <TopBar variant="screenwithicon" title="Search" />
      <View
        style={tailwind(
          'px-4 mb-2 mx-2  border flex flex-row items-center rounded-lg',
        )}>
        <Icon name="search" size={20} color="#FF2E2E" />
        <TextInput
          placeholder="Search Restaurants"
          style={[
            tailwind('flex-grow text-base px-4 py-3 rounded-lg'),
            styles.RegularFont,
          ]}
        />
      <Icon name="close" size={25} color="#FF2E2E" />

      </View>
      <View style={tailwind('flex flex-row justify-between px-3 pb-4')}>
        <Pressable style={[tailwind(' py-2 flex-grow rounded-lg'),styles.BackgroundPrimary]}>
            <Text style={[tailwind('text-white text-center'),styles.SemiBoldFont]}>Restaurants</Text>
        </Pressable>
        <Text style={tailwind('px-6')}></Text>
        <Pressable style={[tailwind(' py-2 flex-grow rounded-lg border')]}>
            <Text style={[tailwind('text-black text-center'),styles.SemiBoldFont]}>Products</Text>
        </Pressable>
      </View>
      {
        empty ? (
          <View style={tailwind('flex flex-col justify-center items-center')}>
            <Image resizeMode={'contain'} style={tailwind('w-60 h-60')} source={require('../assets/images/emptyfavorites.png')}/>
            <Text style={[tailwind('text-center'),styles.SemiBoldFont,styles.ColorPrimary]}>No Restaurant Available Right Now</Text>
        </View>

        )
        :
        <ScrollView>
          <SpotLightRestaurant
            name="Hotel Buhari"
            special="Arabian, Biryani, Chinese, Juices"
            location="Purasaiwakkam, 2.2 kms"
            rating="4.5"
            distance="20 mins"
            price="Rs 600 for two"
            offer="offer 34%"
          />

          <SpotLightRestaurant
            name="Hotel Buhari"
            special="Arabian, Biryani, Chinese, Juices"
            location="Purasaiwakkam, 2.2 kms"
            rating="4.5"
            distance="20 mins"
            price="Rs 600 for two"
            offer="offer 34%"
          />

        </ScrollView>
      }

    </View>
  );
}
