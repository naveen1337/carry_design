export let styles = {
  BackgroundPrimary: {
    backgroundColor: '#FF2E2E',
  },
  BackgroundSecondary: {
    backgroundColor: '#56155B',
  },
  ColorPrimary: {
    color: '#FF2E2E',
  },
  ColorSecondary: {
    color: '#56155B',
  },
  BackgroundGray: {
    backgroundColor: '#58585B',
  },
  ColorGray: {
    color: '#58585B',
  },
  ColorBlack: {
    color: '#404041',
  },
  BackgroundPurpleDark: {
    backgroundColor: '#56155B',
  },
  BackgroundPurpleLight: {
    backgroundColor: '#A86398',
  },
  ShadowBox: {
    elevation: 2,
  },
  RegularFont: {
    fontFamily: 'JosefinSans-Regular',
  },
  MediumFont: {
    fontFamily: 'JosefinSans-Medium',
  },
  SemiBoldFont: {
    fontFamily: 'JosefinSans-SemiBold',
  },
  BoldFont: {
    fontFamily: 'JosefinSans-Bold',
  },
  px14: {
    fontSize: 14,
  },
  px16: {
    fontSize: 14,
  },
  px17: {
    fontSize: 14,
  },
  px18: {
    fontSize: 14,
  },
  px8: {
    fontSize: 8,
  },
  px12: {
    fontSize: 12,
  },
};
