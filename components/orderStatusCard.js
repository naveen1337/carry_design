import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function OrderStatusCard(props) {
  return (
    <View style={tailwind('mx-2 px-5 py-4')}>
      <View
        style={tailwind(
          'flex flex-row justify-between items-center border-b-2 pb-2 border-gray-200',
        )}>
        <View style={tailwind('flex flex-row items-center')}>
          <Text
            style={[
              tailwind('text-base'),
              styles.ColorPrimary,
              styles.SemiBoldFont,
            ]}>
            OID
          </Text>
          <Text
            style={[
              tailwind('text-base px-3'),
              styles.ColorBlack,
              styles.RegularFont,
            ]}>
            {props.id}
          </Text>
        </View>
        <Text
          style={[tailwind('text-xs'), styles.ColorBlack, styles.RegularFont]}>
          {props.time}
        </Text>
      </View>
      <View style={tailwind('flex flex-row justify-between py-4')}>
        <View>
          <Image
            resizeMode={'contain'}
            style={tailwind('rounded-full w-12 h-12')}
            source={require('../assets/images/success.png')}
          />
          <Text
            style={[tailwind('text-center  text-xs py-1'), styles.RegularFont]}>
            Placed
          </Text>
        </View>

        <View>
          {props.status > 1 ? (
            <Image
              resizeMode={'contain'}
              style={tailwind('rounded-full w-12 h-12')}
              source={require('../assets/images/success.png')}
            />
          ) : (
            <Image
              resizeMode={'contain'}
              style={tailwind('rounded-full w-12 h-12')}
              source={require('../assets/images/packed.png')}
            />
          )}

          <Text
            style={[tailwind('text-center  text-xs py-1'), styles.RegularFont]}>
            Packed
          </Text>
        </View>
        <View>
          {props.status > 2 ? (
            <Image
              resizeMode={'contain'}
              style={tailwind('rounded-full w-12 h-12')}
              source={require('../assets/images/success.png')}
            />
          ) : (
            <Image
              resizeMode={'contain'}
              style={tailwind('rounded-full w-12 h-12')}
              source={require('../assets/images/ontheway.png')}
            />
          )}

          <Text
            style={[tailwind('text-center text-xs py-1'), styles.RegularFont]}>
            Pick Up
          </Text>
        </View>
        <View>
          {props.status > 3 ? (
            <Image
              resizeMode={'contain'}
              style={tailwind('rounded-full w-12 h-12')}
              source={require('../assets/images/success.png')}
            />
          ) : (
            <Image
              resizeMode={'contain'}
              style={tailwind('rounded-full w-12 h-12')}
              source={require('../assets/images/delivered.png')}
            />
          )}

          <Text
            style={[tailwind('text-center  text-xs py-1'), styles.RegularFont]}>
            Delivered
          </Text>
        </View>
      </View>
      <Text
        style={[
          tailwind('text-base'),
          styles.SemiBoldFont,
          styles.ColorPrimary,
        ]}>
        Status
      </Text>
      <Text style={[tailwind('text-sm'), styles.RegularFont]}>
        Your Restaurant is yet to confirm your order
      </Text>
    </View>
  );
}
