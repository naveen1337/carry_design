import React from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  Pressable,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function DeliveryRoute(props) {
  let [cod, setcod] = React.useState(false);
  let [razorpay, setrazorpay] = React.useState(false);

  function toggele(e) {
    setcod(false);
    setrazorpay(false);
    if (e === 'cod') {
      setcod(true);
    }
    if (e === 'razorpay') {
      setrazorpay(true);
    }
  }

  return (
    <View>
      <ScrollView style={[tailwind('mx-4')]}>
        <View style={tailwind('flex flex-row justify-start items-center')}>
          <Image
            resizeMode={'contain'}
            style={tailwind('w-6 h-6')}
            source={require('../assets/icons/food.png')}
          />
          <View style={tailwind('px-4')}>
            <Text
              style={[
                tailwind('text-lg'),
                styles.ColorPrimary,
                styles.BoldFont,
              ]}>
              {props.from}
            </Text>
            <Text
              style={[
                tailwind('text-sm'),
                styles.ColorGray,
                styles.RegularFont,
              ]}>
              {props.frominfo}
            </Text>
          </View>
        </View>

        <Icon
          style={tailwind('px-1')}
          name="ellipsis-vertical-outline"
          size={20}
          color="#FF2E2E"
        />

        <View style={tailwind('flex flex-row justify-start items-center')}>
          <Image
            resizeMode={'contain'}
            style={tailwind('w-7 h-7')}
            source={require('../assets/icons/location.png')}
          />
          <View style={tailwind('px-4')}>
            <Text
              style={[
                tailwind('text-lg'),
                styles.ColorPrimary,
                styles.BoldFont,
              ]}>
              My Address
            </Text>
            <Text
              style={[
                tailwind('text-sm'),
                styles.ColorGray,
                styles.RegularFont,
              ]}>
              {props.toaddress}
            </Text>
          </View>
        </View>

        <View style={tailwind('px-2 mt-6 mb-2')}>
          <Text
            style={[
              tailwind('text-base'),
              styles.ColorPrimary,
              styles.SemiBoldFont,
            ]}>
            Payment Method
          </Text>
        </View>

        <Pressable
          onPress={() => toggele('cod')}
          style={tailwind(
            'flex flex-row px-2 justify-between bg-gray-200 py-3 my-2 rounded-lg',
          )}>
          <Text
            style={[
              tailwind('text-lg'),
              styles.ColorBlack,
              styles.SemiBoldFont,
            ]}>
            Cash on Delivery
          </Text>
          {cod ? (
            <Icon name="radio-button-on-outline" size={25} color="red" />
          ) : (
            <Icon name="radio-button-off-outline" size={25} color="red" />
          )}
        </Pressable>

        <Pressable
          onPress={() => toggele('razorpay')}
          style={tailwind(
            'flex flex-row px-2 justify-between bg-gray-200 py-3 my-2 rounded-lg',
          )}>
          <Text
            style={[
              tailwind('text-lg'),
              styles.ColorBlack,
              styles.SemiBoldFont,
            ]}>
            RazorPay
          </Text>
          {razorpay ? (
            <Icon name="radio-button-on-outline" size={25} color="red" />
          ) : (
            <Icon name="radio-button-off-outline" size={25} color="red" />
          )}
        </Pressable>
      </ScrollView>
    </View>
  );
}
