import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function NotificationBanner(props) {
  return (
    <View style={tailwind('py-3  flex flex-row justify-start')}>
      <Image
        resizeMode={'contain'}
        style={tailwind('w-12 h-12 ')}
        source={require('../assets/icons/delivery.png')}
      />
      <Text
        style={[
          tailwind('text-sm w-64 pl-2'),
          styles.SemiBoldFont,
          styles.ColorBlack,
        ]}>
        {props.text}
      </Text>
    </View>
  );
}
