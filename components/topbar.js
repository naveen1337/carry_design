import React from 'react';
import {View, Text, StyleSheet, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function RestaurantScreen(props) {
  if (props.varient == 'restaurant') {
    return (
      <View>
        <View
          style={[
            tailwind('py-4 px-4 flex flex-row justify-between'),
            styles.ShadowBox,
          ]}>
          <Pressable>
            <Icon name="chevron-back-circle" size={24} color="red" />
          </Pressable>

          <View style={tailwind('flex-row')}>
            <Icon
              style={tailwind('px-2')}
              name="heart-outline"
              size={24}
              color="black"
            />
            <Icon
              style={tailwind('px-2')}
              name="search-outline"
              size={24}
              color="black"
            />
          </View>
        </View>
      </View>
    );
  }
  if (props.variant === 'screen') {
    return (
      <View>
        <View
          style={[
            tailwind('py-4 px-4 flex flex-row items-center'),
            styles.ShadowBox,
          ]}>
          <Pressable>
            <Icon name="chevron-back-circle" size={30} color="red" />
          </Pressable>

          <Text
            style={[
              tailwind('text-lg px-3'),
              styles.ColorPrimary,
              styles.SemiBoldFont,
            ]}>
            {props.text}
          </Text>
        </View>
      </View>
    );
  }

  if (props.variant === 'screenwithicon') {
    return (
      <View style={[tailwind('py-4 mx-4 flex bg-white flex-row items-center')]}>
        <Icon name="chevron-back-circle" size={30} color="#FF2E2E" />
        <Text
          style={[
            styles.SemiBoldFont,
            styles.ColorPrimary,
            tailwind('text-lg mx-3'),
          ]}>
          {props.title}
        </Text>
      </View>
    );
  }

  if (props.variant === 'screenwithicontext') {
    return (
      <View
        style={[
          tailwind('py-4 mx-4 flex flex-row justify-between items-center'),
        ]}>
        <View style={tailwind('flex flex-row')}>
          <Icon name="chevron-back-circle" size={30} color="#FF2E2E" />
          <Text
            style={[
              styles.SemiBoldFont,
              styles.ColorPrimary,
              tailwind('text-lg mx-3'),
            ]}>
            {props.title}
          </Text>
        </View>
        <Pressable
          style={[
            tailwind('px-3 py-2 rounded rounded-lg'),
            styles.BackgroundPrimary,
          ]}>
          <Text
            style={[
              tailwind('uppercase text-white uppercase'),
              styles.RegularFont,
            ]}>
            Skip
          </Text>
        </Pressable>
      </View>
    );
  }
}
