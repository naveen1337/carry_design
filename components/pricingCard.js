import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function PricingCart(props) {
  return (
    <View style={tailwind('mx-4 py-3 border border-gray-300 px-3 rounded-xl')}>
      <View style={tailwind('flex flex-row justify-between py-1')}>
        <Text
          style={[tailwind('text-sm text-indigo-900'), styles.SemiBoldFont]}>
          Sub total
        </Text>
        <Text
          style={[tailwind('text-sm text-indigo-900'), styles.SemiBoldFont]}>
          ₹ {props.total}
        </Text>
      </View>

      <View style={tailwind('flex flex-row justify-between')}>
        <Text
          style={[tailwind('text-sm text-gray-500 py-1'), styles.SemiBoldFont]}>
          Delivery Charge
        </Text>
        <Text style={[tailwind('text-sm text-gray-500'), styles.SemiBoldFont]}>
          ₹ {props.delivery}
        </Text>
      </View>

      <View style={tailwind('flex flex-row justify-between')}>
        <Text
          style={[tailwind('text-sm text-gray-700 py-1'), styles.SemiBoldFont]}>
          Package Charge
        </Text>
        <Text style={[tailwind('text-sm text-gray-700'), styles.SemiBoldFont]}>
          ₹ {props.packing}
        </Text>
      </View>
      <View style={tailwind('flex flex-row justify-between')}>
        <Text
          style={[
            tailwind('text-sm text-green-500 py-1'),
            styles.SemiBoldFont,
          ]}>
          Tax and Charges
        </Text>
        <Text style={[tailwind('text-sm text-green-500'), styles.SemiBoldFont]}>
          ₹ {props.tax}
        </Text>
      </View>
    </View>
  );
}
