import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function cartaddress() {
  return (
    <View
      style={[
        tailwind(
          'mx-3 mt-4 rounded-lg p-2 flex flex-row items-center justify-between',
        ),
        styles.BackgroundPrimary,
      ]}>
      <Image
        resizeMode={'contain'}
        style={tailwind('w-8 h-8')}
        source={require('../assets/images/gps.png')}
      />
      <View style={tailwind(' w-7/12')}>
        <Text style={[tailwind('text-white'), styles.SemiBoldFont]}>
          Deliver to
        </Text>
        <Text style={[tailwind('text-white text-xs mt-1'), styles.RegularFont]}>
          #120, Purasaiwalkkam Main Road, Doveton
        </Text>
      </View>
      <View style={tailwind('flex flex-row pl-2 items-center')}>
        <Text
          style={[tailwind('text-white text-xs uppercase'), styles.BoldFont]}>
          Change
        </Text>
        <Icon
          style={tailwind('')}
          name="chevron-forward"
          size={25}
          color="#ffffff"
        />
      </View>
    </View>
  );
}
