import React, {useState} from 'react';
import {Pressable, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import Collapsible from 'react-native-collapsible';
import ProductCard from '../components/productcard';
import {styles} from '../constants/styles';

export default function CategoryTitle(props) {
  let [collapse, setcollapse] = useState(true);
  let [product, setproduct] = useState([
    {
      id: '50',
      city: 'Chennai',
      restaurant_id: '3',
      category_id: '3',
      category_name: 'Fired chicken',
      product_name: ' Hot & Crispy Chicken',
      description: '2 Pc Hot & Crispy Chicken',
      product_status: '1',
      product_message: 'Available',
      best_seller: 1,
      img_url:
        'https://i.pinimg.com/originals/17/47/49/1747495edb34a252d88b2a2059843185.jpg?epik=dj0yJnU9S0ZMRTJKMnZ1bUNyOEt2ektuRGxoNkhxcTZObWVZWWwmcD0wJm49Z3VZa0VJbEpNMkxuRWIyR2lvekFDdyZ0PUFBQUFBR0Fhb1Mw',
      variation: 'true',
      price_variation: [
        {
          variation_id: '149',
          variation: '1 bkt',
          demo_amount: '400',
          amount: '360',
          price_offer_percentage: '10 %',
        },
        {
          variation_id: '139',
          variation: '3 bkt',
          demo_amount: '500',
          amount: '460',
          price_offer_percentage: '7 %',
        },
      ],
      addon: true,
      addon_content: [
        {
          id: '2',
          name: 'ketchup',
          price: '10',
        },
        {
          id: '1',
          name: 'Mayonnaise',
          price: '20',
        },
        {
          id: '245',
          name: 'Test 1',
          price: '10',
        },
        {
          id: '56',
          name: 'Test 2',
          price: '10',
        },
      ],
      demoamt: '0',
      price: '0',
      price_offer_percentage: '100 %',
      status: '1',
      recom: '1',
      type: '0',
    },
  ]);
  return (
    <View>
      <Pressable
        onPress={() => setcollapse(!collapse)}
        style={tailwind(
          'flex px-3 flex-row justify-between pt-6 pb-2 border-t-2 border-b-2 border-gray-200',
        )}>
        <Text style={[tailwind('text-base'), styles.BoldFont]}>
          {props.title}
        </Text>
        {collapse ? (
          <Icon name="chevron-down-outline" size={25} color="#FF2E2E" />
        ) : (
          <Icon name="chevron-up-outline" size={25} color="#FF2E2E" />
        )}
      </Pressable>
      <Collapsible style={tailwind('')} collapsed={collapse}>
        {product.map((item) => {
          return (
            <ProductCard
              key={item.id}
              hasdesc={true}
              hasimage={false}
              hasvariation={true}
              bestseller={false}
              veg={false}
              status={true}
              id={item.id}
              image={item.img_url}
              name={item.product_name}
              desc={item.description}
              variation={item.price_variation}
              hasaddon={item.addon}
              addons={item.addon_content}
              price={item.price}
            />
          );
        })}
      </Collapsible>
    </View>
  );
}
