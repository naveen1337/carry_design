import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import {mapStyle} from '../constants/mapstyle';

import MapView, {
  Marker,
  Callout,
  UrlTile,
  PROVIDER_GOOGLE,
} from 'react-native-maps';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';


export default function MapViewComponent(props) {
  return (
    <MapView
      style={{height: '50%'}}
      customMapStyle={mapStyle}
      initialRegion={{
        latitude: props.userlat,
        longitude: props.userlong,
        latitudeDelta: 0,
        longitudeDelta: 0.01,
      }}>
      <Marker
        coordinate={{
          latitude: props.userlat,
          longitude: props.userlong,
          latitudeDelta: 0.0043,
          longitudeDelta: 0.0034,
        }}
        title="Your Current Location"
        description="Sample Text in the card">
        <Image
          resizeMode={'contain'}
          style={tailwind('')}
          source={require('../assets/icons/mapuser.png')}
        />
      </Marker>
    </MapView>
  );
}
