import React from 'react';
import {View, Text, Pressable, Image} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function SpotilightTitle(props) {
  return (
    <View style={tailwind('mx-4 py-4 flex flex-row items-center')}>
      {props.foodicon ? (
        <Icon
          style={tailwind('mx-1')}
          name="fast-food-outline"
          size={25}
          color="#FF2E2E"
        />
      ) : (
        <Image
          style={tailwind('w-4 h-6 mx-2')}
          source={require('../assets/icons/red_ribbon.png')}
        />
      )}

      <View>
        <Text
          style={[tailwind('text-lg mx-1 text-gray-800'), styles.SemiBoldFont]}>
          {props.title}
        </Text>
        {props.subtitle ? (
          <Text
            style={[
              tailwind('text-xs mx-1'),
              styles.RegularFont,
              styles.ColorGray,
            ]}>
            {props.subtitle}
          </Text>
        ) : (
          <></>
        )}
      </View>
    </View>
  );
}
