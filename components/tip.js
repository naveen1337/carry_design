import React from 'react';
import {
  View,
  Text,
  Image,
  Pressable,
  StyleSheet,
  TextInput,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function Tip() {
  const [toggle, settoggle] = React.useState(false);
  return (
    <View style={tailwind('pt-3 flex flex-row')}>
      <Icon name="leaf-outline" size={35} color="#FF2E2E" />
      <View style={tailwind('px-3')}>
        <Text
          style={[tailwind('text-lg'), styles.RegularFont, styles.ColorBlack]}>
          Tip the partner!
        </Text>
        <Text style={customstyles.text}>
          Our Carry delivery person will call you (or ring your doorbell) once
          the order is placed at your door. (Not applicable for COD)
        </Text>
        <View
          style={tailwind('flex flex-row mt-3 justify-between items-center')}>
          <Pressable
            style={[
              tailwind('border rounded-lg py-1 border-gray-500'),
              customstyles.buttonwidth,
            ]}>
            <Text
              style={[
                tailwind('text-center px-2'),
                styles.RegularFont,
                styles.ColorGray,
              ]}>
              10
            </Text>
          </Pressable>

          <Pressable
            style={[
              tailwind('border rounded-lg py-1  border-gray-500'),
              customstyles.buttonwidth,
            ]}>
            <Text
              style={[
                tailwind('text-center px-2'),
                styles.RegularFont,
                styles.ColorGray,
              ]}>
              20
            </Text>
          </Pressable>

          <Pressable
            style={[
              tailwind('border rounded-lg py-1 border-gray-500'),
              customstyles.buttonwidth,
            ]}>
            <Text
              style={[
                tailwind('text-center px-2'),
                styles.RegularFont,
                styles.ColorGray,
              ]}>
              10
            </Text>
          </Pressable>

          <Pressable
            onPress={() => settoggle(!toggle)}
            style={[tailwind('border rounded-lg py-1 border-gray-500')]}>
            <Text
              style={[
                tailwind('text-center px-2'),
                styles.RegularFont,
                styles.ColorGray,
              ]}>
              Other
            </Text>
          </Pressable>
        </View>
        {toggle ? (
          <View style={tailwind('flex flex-row items-center')}>
            <TextInput style={customstyles.inputbox} />
            <Pressable
              style={[
                tailwind('py-2 px-3 mx-3 mt-3 rounded-lg'),
                styles.BackgroundPrimary,
              ]}>
              <Text style={[tailwind('text-red-100'), styles.SemiBoldFont]}>
                Submit
              </Text>
            </Pressable>
          </View>
        ) : (
          <></>
        )}
      </View>
    </View>
  );
}

const customstyles = StyleSheet.create({
  text: {
    fontSize: 8,
    color: '#58585B',
    width: Dimensions.get('window').width - 100,
    marginVertical: 5,
  },
  inputbox: {
    marginTop: 12,
    paddingVertical: 5,
    borderRadius: 10,
    borderStyle: 'solid',
    borderWidth: 1,
    backgroundColor: '#F6F6F6',
    width: Dimensions.get('window').width - 170,
  },
  buttonwidth: {
    width: '20%',
  },
});
