import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function HomeFooter() {
  return (
    <View style={tailwind('mx-4 my-4 flex flex-row justify-between items-center')}>
      <View>
        <View style={tailwind('flex flex-row')}>
          <Text
            style={[
              tailwind('text-xl pr-2 font-bold'),
              styles.BoldFont,
              styles.ColorPrimary,
            ]}>
            We
          </Text>
          <Text
            style={[
              tailwind('text-xl pr-2'),
              styles.RegularFont,
              styles.ColorPrimary,
            ]}>
            Carry
          </Text>
        </View>
        <Text
          style={[
            tailwind('text-xl pr-2'),
            styles.BoldFont,
            styles.ColorPrimary,
          ]}>
        Anything for you
        </Text>
      </View>
      <Image
        resizeMode={'contain'}
        style={tailwind('w-16 h-16')}
        source={require('../assets/icons/delivery2.png')}
      />
    </View>
  );
}
