import React from 'react';
import {
  View,
  Text,
  Image,
  Pressable,
  Dimensions,
  StyleSheet,
} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import {SliderBox} from 'react-native-image-slider-box';

export default function Tip() {
  let [images, setimages] = React.useState([
    require('../assets/images/tempbanner1.png'),
    require('../assets/images/tempbanner2.png'),
    require('../assets/images/tempbanner1.png'),
    require('../assets/images/tempbanner2.png'),
  ]);
  return (
    <View style={[tailwind('flex flex-row justify-center')]}>
      <SliderBox
        images={images}
        parentWidth={Dimensions.get('window').width - 30}
        dotColor="#56155B"
        sliderBoxHeight={156}
        inactiveDotColor="#8A1A4D"
        resizeMethod={'resize'}
        resizeMode={'contain'}
        paginationBoxStyle={{
          position: 'relative',
          bottom: 0,
          padding: 0,
          alignItems: 'center',
          alignSelf: 'center',
          justifyContent: 'center',
          paddingVertical: 10,
        }}
        dotStyle={{
          width: 30,
          height: 4,
          //   borderRadius: 5,
          marginHorizontal: 0,
          padding: 0,
          margin: 0,
          backgroundColor: 'red',
        }}
      />
    </View>
  );
}

const customstyle = StyleSheet.create({
  width: {
    // width: '90%',
  },
});
