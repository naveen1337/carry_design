import React from 'react';
import {View, Text, Pressable, Image} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function restaurantcard(props) {
  return (
    <View style={tailwind('mx-5 py-1')}>
      <View style={tailwind('flex flex-row py-3 border-b-2 border-red-200')}>
        <Image
          resizeMode={'contain'}
          style={tailwind('w-20 h-20')}
          source={{uri: props.image}}
        />
        <View>
          <View style={tailwind('px-3 flex flex-row items-center')}>
            <Text
              style={[
                tailwind('text-lg'),
                styles.ColorPrimary,
                styles.SemiBoldFont,
              ]}>
              {props.brandname}
            </Text>
            <Text
              style={[
                tailwind('px-3 py-1 mx-2 text-xs rounded-lg text-white'),
                styles.RegularFont,
                styles.BackgroundPrimary,
              ]}>
              OPEN
            </Text>
          </View>
          <Text style={[tailwind('px-3 text-gray-600'), styles.RegularFont]}>
            {props.special}
          </Text>
        </View>
      </View>

      <View style={tailwind('flex flex-row justify-around py-2')}>
        <View>
          <View style={tailwind('flex flex-row justify-center items-center')}>
            <Icon name="star" size={15} color="#56155B" />
            <Text style={[tailwind('text-sm px-1'), styles.SemiBoldFont]}>
              {props.rating}
            </Text>
          </View>
          <Text
            style={[
              tailwind('text-center'),
              styles.RegularFont,
              styles.ColorGray,
              styles.px8,
            ]}>
            Packaging 91%
          </Text>
        </View>

        <View>
          <View style={tailwind('flex flex-row justify-center items-center')}>
            <Text
              style={[
                tailwind('text-sm px-1'),
                styles.SemiBoldFont,
                styles.ColorPrimary,
              ]}>
              {props.deliverytime}
            </Text>
          </View>
          <Text
            style={[
              tailwind('text-center text-xs'),
              styles.RegularFont,
              styles.ColorGray,
              styles.px8,
            ]}>
            Delivery Time
          </Text>
        </View>
        <View>
          <View style={tailwind('flex flex-row justify-center items-center')}>
            <Text
              style={[
                tailwind('text-sm px-1'),
                styles.SemiBoldFont,
                styles.ColorSecondary,
              ]}>
              Rs.{props.price}
            </Text>
          </View>
          <Text
            style={[
              tailwind('text-center text-xs'),
              styles.RegularFont,
              styles.ColorGray,
              styles.px8,
            ]}>
            Cost for 2
          </Text>
        </View>
      </View>
      {/* Info Banner  */}

      <View
        style={[
          tailwind('rounded flex flex-row  justify-center my-2 items-center'),
          styles.BackgroundPrimary,
        ]}>
        <View style={tailwind('flex flex-row py-2 items-center')}>
          <Icon
            style={tailwind('pr-2 pt-1')}
            name="reader"
            size={15}
            color="#ffffff"
          />
          <Text style={[tailwind('text-white text-xs '), styles.RegularFont]}>
            This restaurant follows Best Security Standards
          </Text>
          <Icon
            style={tailwind('pl-2 pt-1')}
            name="chevron-forward-outline"
            size={15}
            color="#ffffff"
          />
        </View>
      </View>

      {/* End of Info Banner */}
    </View>
  );
}
