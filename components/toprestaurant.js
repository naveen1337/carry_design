import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function TopRestaurant(props) {
  return (
    <View style={tailwind('w-24 my-2 mr-2')}>
      <Image
        resizeMode={'cover'}
        style={tailwind('w-24 h-24 rounded-lg')}
        source={require('../assets/images/sample.jpg')}
      />
      {props.hasoffer ? (
        <>
          <Text
            style={[
              tailwind(
                'relative bottom-3 mx-2 uppercase rounded text-center text-red-100 py-1',
              ),
              styles.BackgroundPrimary,
              styles.RegularFont,
            ]}>
            {props.offer}
          </Text>
          <View style={tailwind(' relative bottom-2 mx-1')}>
            <Text
              numberOfLines={2}
              style={[
                tailwind('text-sm text-gray-800 w-20'),
                styles.SemiBoldFont,
              ]}>
              {props.name}
            </Text>
            <Text
              numberOfLines={1}
              style={[tailwind('text-xs text-gray-700'), styles.SemiBoldFont]}>
              {props.time}
            </Text>
          </View>
        </>
      ) : (
        <>
          <View style={tailwind(' mx-1')}>
            <Text
              numberOfLines={2}
              style={[
                tailwind('text-sm text-gray-800 w-20'),
                styles.SemiBoldFont,
              ]}>
              {props.name}
            </Text>
            <Text
              numberOfLines={1}
              style={[tailwind('text-xs text-gray-700'), styles.SemiBoldFont]}>
              {props.time}
            </Text>
          </View>
        </>
      )}
    </View>
  );
}
