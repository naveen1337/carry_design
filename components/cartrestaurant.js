import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function cartrestaurant(props) {
  return (
    <View style={tailwind('mx-2 flex flex-row flex-grow')}>
      <Image
        style={tailwind('w-20 h-20 rounded rounded-lg')}
        source={require('../assets/images/sample.jpg')}
      />
      <View style={tailwind('px-2')}>
        <Text
          style={[tailwind('text-base'), styles.BoldFont, styles.ColorPrimary]}>
          {props.name}
        </Text>
        <Text style={[tailwind('text-sm'), styles.RegularFont, styles.ColorGray]}>
          {props.special}
        </Text>
        <Text style={[tailwind('text-xs'), styles.RegularFont, styles.ColorGray]}>
          {props.location}
        </Text>
      </View>
    </View>
  );
}
