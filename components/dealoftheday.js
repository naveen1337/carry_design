import React from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  StyleSheet,
  Pressable,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function DealoftheDay(props) {
  return (
    <View style={[tailwind('mx-1'), mystyles.width]}>
      <Image
        resizeMode={'cover'}
        style={[tailwind('rounded-lg h-36'), mystyles.width]}
        source={require('../assets/images/sample.jpg')}
      />
      <Text style={[tailwind('text-base pt-1 text-center'),styles.SemiBoldFont,styles.ColorBlack]}>
          {props.title}
      </Text>
      <Text style={[tailwind('text-xs pt-1 text-center'),styles.SemiBoldFont,styles.ColorGray]}>
          {props.subtitle}
      </Text>
    </View>
  );
}

const mystyles = StyleSheet.create({
  width: {
    width: Dimensions.get('window').width / 2.25,
  },
  height: {
    height: Dimensions.get('window').width / 2.2,
  },
});
