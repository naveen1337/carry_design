import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function SpotilightRestaurant(props) {
  return (
    <View style={tailwind('mx-3 my-2 flex flex-row justify-start')}>
      <View style={tailwind('')}>
        <Image
          style={tailwind('w-28 h-32 rounded rounded-lg')}
          source={require('../assets/images/sample.jpg')}
        />
        <Text
          style={[
            tailwind(
              'absolute top-2 text-center uppercase text-red-100 w-28 py-1 bg-red-500',
            ),
            styles.BackgroundPrimary,
            styles.SemiBoldFont,
          ]}>
         {props.offer}
        </Text>

        <View
          style={[
            tailwind(
              'relative mx-1 bottom-4 flex flex-row justify-center rounded px-1',
            ),
            styles.BackgroundPrimary,
          ]}>
          <Text
            style={[
              tailwind('text-center uppercase text-red-100 py-1 '),

              styles.SemiBoldFont,
            ]}>
            spotlight
          </Text>
        </View>
      </View>
      <View style={tailwind('px-2 flex-grow')}>
        <Text
          style={[
            tailwind('text-xl'),
            styles.SemiBoldFont,
            styles.ColorPrimary,
          ]}>
          {props.name}
        </Text>
        <Text
          numberOfLines={1}
          style={[
            tailwind('text-xs w-11/12'),
            styles.RegularFont,
            styles.ColorGray,
          ]}>
          {props.special}
        </Text>
        <Text
          numberOfLines={1}
          style={[
            tailwind('text-xs w-11/12 py-1'),
            styles.RegularFont,
            styles.ColorGray,
          ]}>
          {props.location}
        </Text>
        <View
          style={tailwind(
            'flex border-b-2 flex-grow pb-2 border-red-500 flex-row justify-between items-center',
          )}>
          <Image
            style={tailwind('w-4 h-4 px-1')}
            source={require('../assets/icons/star.png')}
          />
          <Text
            style={[
              tailwind('pl-1 text-sm'),
              styles.SemiBoldFont,
              styles.ColorBlack,
            ]}>
            {props.rating}
          </Text>
          <Text
            style={[
              tailwind('pl-1 text-sm'),
              styles.SemiBoldFont,
              styles.ColorBlack,
            ]}>
            |
          </Text>
          <Text
            style={[
              tailwind('pl-1 text-sm'),
              styles.SemiBoldFont,
              styles.ColorBlack,
            ]}>
            {props.distance}
          </Text>
          <Text
            style={[
              tailwind('pl-1 text-sm'),
              styles.SemiBoldFont,
              styles.ColorBlack,
            ]}>
            |
          </Text>
          <Text
            style={[
              tailwind('pl-1 text-sm'),
              styles.SemiBoldFont,
              styles.ColorBlack,
            ]}>
            {props.price}
          </Text>
        </View>
        <View style={tailwind('pt-1 flex flex-row items-center')}>
          <Image
            style={tailwind('w-8 h-8')}
            source={require('../assets/icons/ring.png')}
          />
          <Text
            style={[
              tailwind('text-xs w-28 flex-grow '),
              styles.RegularFont,
              styles.ColorGray,
            ]}>
            Top brand, brought near you by Carry
          </Text>
        </View>
      </View>
    </View>
  );
}
