import React from 'react';
import {View, Text, Image, Pressable, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function CheckOutButton(props) {
  return (
    <View style={tailwind('flex flex-row justify-between items-center')}>
      <Pressable
        style={[
          tailwind('h-full py-2 w-6/12 bg-gray-400'),
          styles.BackgroundPurpleLight,
        ]}>
        <Text
          style={[tailwind('text-base text-white mx-2'), styles.RegularFont]}>
          ₹ {props.amount}
        </Text>
        <Text
          style={[tailwind(' text-xs text-white mx-2'), styles.RegularFont]}>
          View Total Invoice
        </Text>
      </Pressable>
      <Pressable
        style={[
          tailwind('h-full flex flex-col items-center justify-center w-6/12 '),
          styles.BackgroundPurpleDark,
        ]}>
        <Text
          style={[
            tailwind('text-center text-base text-white uppercase'),
            styles.RegularFont,
          ]}>
          Proceed to pay
        </Text>
      </Pressable>
    </View>
  );
}
