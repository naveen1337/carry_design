import React from 'react';
import {View, Text, Image, Pressable, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function Tip(props) {
  return (
    <View
      style={tailwind(
        'border-b-2 mx-4 border-gray-200 flex flex-row items-center justify-between  py-2  ',
      )}>
      <Image
        resizeMode={'contain'}
        style={tailwind('w-6 h-6')}
        source={require('../assets/icons/location.png')}
      />
      <View>
        <Text
          style={[
            tailwind('text-sm pb-1 '),
            styles.SemiBoldFont,
            styles.ColorPrimary,
          ]}>
          My Address
        </Text>
        <Text
          style={[
            tailwind('text-xs mr-2 '),
            styles.SemiBoldFont,
            styles.ColorGray,
          ]}>
          {props.address}
        </Text>
      </View>
      <View style={tailwind('flex flex-row items-center')}>
        <Image
          resizeMode={'contain'}
          style={tailwind('w-6 h-6')}
          source={require('../assets/icons/offer.png')}
        />
        <Text
          style={[
            tailwind('text-sm px-1'),
            styles.SemiBoldFont,
            styles.ColorPrimary,
          ]}>
          Offers
        </Text>
      </View>
    </View>
  );
}
