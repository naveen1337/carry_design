import React from 'react';
import {View, Text, TextInput, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function OrderDetails(props) {
  return (
    <View style={tailwind('my-3 p-3 rounded mx-3 border border-gray-300')}>
      <View style={tailwind('flex flex-row justify-between')}>
        <View style={tailwind('flex flex-row')}>
          <Text
            style={[
              tailwind('text-sm'),
              styles.SemiBoldFont,
              styles.ColorPrimary,
            ]}>
            OID
          </Text>
          <Text
            style={[
              tailwind('text-sm px-3'),
              styles.SemiBoldFont,
              styles.ColorBlack,
            ]}>
            {props.orderid}
          </Text>
        </View>
        <View style={tailwind('flex flex-row items-center')}>
          {props.completed ? (
            <Text
              style={[
                tailwind('text-xs text-green-600 px-2'),
                styles.SemiBoldFont,
              ]}>
              Placed
            </Text>
          ) : (
            <Text
              style={[
                tailwind('text-xs text-green-600 px-2'),
                styles.SemiBoldFont,
              ]}>
              Delivered
            </Text>
          )}

          <Image
            source={require('../assets/images/tick.png')}
            style={tailwind('w-4 h-4')}
            resizeMode={'contain'}
          />
        </View>
      </View>
      <View
        style={tailwind(
          'flex flex-row items-center py-2 border-b-2 border-red-200',
        )}>
        <Text style={[tailwind('text-lg pr-2'), styles.SemiBoldFont]}>
          ₹ {props.price}
        </Text>
        <Icon name="chevron-forward" size={25} color="red" />
      </View>
      <Text style={[tailwind('text-sm py-2'), styles.SemiBoldFont]}>
        {props.item}
      </Text>
      <Text style={[tailwind('text-xs py-1'), styles.RegularFont]}>
        {props.time}
      </Text>
      {props.completed ? (
        <View style={tailwind('flex flex-row justify-between py-3')}>
          <View>
            <Text
              style={[
                tailwind('text-gray-500'),
                styles.px12,
                styles.SemiBoldFont,
              ]}>
              Your rating for Delivery
            </Text>
            <View
              style={tailwind('flex flex-row items-center justify-between')}>
              <View style={tailwind('flex flex-row py-2 items-center ')}>
                <Image
                  style={tailwind('w-3 h-3')}
                  resizeMode={'contain'}
                  source={require('../assets/images/star.png')}
                />
                <Text style={[tailwind('px-1'), styles.SemiBoldFont]}>
                  {props.deliveryrating}
                </Text>
              </View>
              <Text
                style={[
                  tailwind('text-gray-600'),
                  styles.px14,
                  styles.ColorBlack,
                  styles.SemiBoldFont,
                ]}>
                Super
              </Text>
            </View>
          </View>

          <View>
            <Text
              style={[
                tailwind('text-gray-500'),
                styles.px12,
                styles.SemiBoldFont,
              ]}>
              Your rating for Food
            </Text>
            <View
              style={tailwind('flex flex-row items-center justify-between')}>
              <View style={tailwind('flex flex-row py-2 items-center ')}>
                <Image
                  style={tailwind('w-3 h-3')}
                  resizeMode={'contain'}
                  source={require('../assets/images/star.png')}
                />
                <Text style={[tailwind('px-1'), styles.SemiBoldFont]}>
                  {props.foodrating}
                </Text>
              </View>
              <Text
                style={[
                  tailwind('text-gray-600'),
                  styles.px14,
                  styles.ColorBlack,
                  styles.SemiBoldFont,
                ]}>
                Ok
              </Text>
            </View>
          </View>
        </View>
      ) : null}
    </View>
  );
}
