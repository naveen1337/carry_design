import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function cartproduct(props) {
  return (
    <View
      elevation={1}
      style={tailwind('mx-3 mt-4 p-2 rounded-lg flex flex-row')}>
      <View style={tailwind('flex flex-col flex-grow justify-between')}>
        <View style={tailwind('flex flex-row flex-grow justify-between')}>
          <Text
            style={[tailwind('text-base'), styles.BoldFont, styles.ColorGray]}>
            {props.name}
          </Text>
        </View>
        {props.hasvariation ? (
          <Text
            style={[
              tailwind('text-xs pb-2'),
              styles.RegularFont,
              styles.ColorGray,
            ]}>
            ({props.variation})
          </Text>
        ) : null}

        <View style={tailwind('flex flex-row items-center justify-between')}>
          <View
            style={[
              tailwind(
                'rounded flex flex-row  justify-between items-center  py-1',
              ),
              styles.BackgroundPrimary,
            ]}>
            <Pressable style={tailwind(' px-1 ')}>
              <Icon name="remove" size={20} color="#ffffff" />
            </Pressable>
            <Text style={[styles.BoldFont, tailwind('text-white px-4')]}>
              {props.quantity}
            </Text>
            <Pressable style={[tailwind('px-1'), styles.BoldFont]}>
              <Icon name="add" size={20} color="#ffffff" />
            </Pressable>
          </View>

          <View style={tailwind('flex flex-col items-center')}>
            {props.hasaddon ? (
              <Text
                style={[
                  tailwind('text-green-600 text-xs'),
                  styles.RegularFont,
                ]}>
                +2 Addons
              </Text>
            ) : null}
            <Text
              style={[
                tailwind('text-base text-black pt-2'),
                styles.SemiBoldFont,
              ]}>
              ₹ {props.totalprice}
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
}
