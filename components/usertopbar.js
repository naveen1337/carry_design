import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function UserTopbar() {
  return (
    <View style={tailwind('mx-2 py-4 flex flex-row justify-between items-center')}>
      <View style={tailwind('flex flex-row items-center')}>
        <Icon name="chevron-back-circle" size={30} color="#FF2E2E" />
        <Text
          style={[
            tailwind('text-base px-2'),
            styles.BoldFont,
            styles.ColorPrimary,
          ]}>
          My Account
        </Text>
      </View>
      <Text
        style={[
          tailwind('text-base px-2 uppercase'),
          styles.BoldFont,
          styles.ColorPrimary,
        ]}>
        Edit
      </Text>
    </View>
  );
}
