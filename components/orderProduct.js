import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function OrderProduct(props) {
  return (
    <View style={tailwind('border border-gray-300 rounded-xl mx-4 my-3 p-5')}>
      <Text style={[tailwind('text-sm py-1'), styles.RegularFont]}>
        {props.name}
      </Text>
      <Text
        style={[tailwind('text-xs py-1 text-gray-500'), styles.RegularFont]}>
        {props.desc}
      </Text>
      <View style={tailwind('flex flex-row justify-between items-center py-1')}>
        <View style={tailwind('flex flex-row items-center')}>
          <Text style={[tailwind('text-sm'), styles.SemiBoldFont]}>
            {props.quantity}
          </Text>
          <Text style={[tailwind('text-sm px-1'), styles.SemiBoldFont]}>x</Text>
          <Text style={[tailwind('text-sm'), styles.SemiBoldFont]}>
            ₹ {props.price}
          </Text>
        </View>

        <Text style={[tailwind('text-sm'), styles.SemiBoldFont]}>
          ₹ {parseInt(props.price) * parseInt(props.quantity)}
        </Text>
      </View>
    </View>
  );
}
