import React, {useState} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function customBottomTab({state, descriptors, navigation}) {
  const [cart, setcart] = useState(8);

  const image1 = '../assets/images/home.png';
  const image2 = '../assets/images/search.png';
  const image3 = '../assets/images/carticon.png';
  const image4 = '../assets/images/user.png';

  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <View style={{flexDirection: 'row', backgroundColor: '#56155B'}}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];

        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            key={index}
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{flex: 1, paddingVertical: 7}}>
            <View style={tailwind('flex flex-col justify-center items-center')}>
              {index === 0 ? (
                <Image
                  resizeMode={'contain'}
                  style={tailwind('w-6 h-6')}
                  source={require(image1)}
                />
              ) : null}
              {index === 1 ? (
                <Image
                  resizeMode={'contain'}
                  style={tailwind('w-6 h-6')}
                  source={require(image2)}
                />
              ) : null}

              {index === 2 ? (
                <>
                  <Image
                    resizeMode={'contain'}
                    style={tailwind('w-6 h-6')}
                    source={require(image3)}
                  />
                  {cart > 0 ? (
                    <View
                      style={[
                        tailwind(
                          'absolute bottom-7 text-base bg-gray-300 rounded-full',
                        ),
                        {
                          right: 30,
                          backgroundColor: '#FF2E2E',
                          paddingHorizontal: 6,
                        },
                      ]}>
                      <Text
                        style={[tailwind('text-white'), styles.RegularFont]}>
                        {cart}
                      </Text>
                    </View>
                  ) : null}
                </>
              ) : null}

              {index === 3 ? (
                <Image
                  resizeMode={'contain'}
                  style={tailwind('w-6 h-6')}
                  source={require(image4)}
                />
              ) : null}

              <Text style={[{color: '#F8CF11'}, styles.RegularFont]}>
                {label}
              </Text>
            </View>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}
