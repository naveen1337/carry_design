import React from 'react';
import {View, Text, Image, Pressable, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function PastOrders(props) {
  return (
    <View style={tailwind('mx-4')}>
      <View style={tailwind('flex flex-row justify-between items-center')}>
        <Text
          style={[
            tailwind('text-base'),
            styles.SemiBoldFont,
            styles.ColorBlack,
          ]}>
          {props.name}
        </Text>
        <View style={tailwind('flex flex-row items-center')}>
          {props.status === 'delivered' ? (
            <>
              <Text
                style={[
                  tailwind('px-3'),
                  styles.SemiBoldFont,
                  styles.ColorBlack,
                ]}>
                Delivered
              </Text>
              <Image
                resizeMode={'contain'}
                style={tailwind('w-6 h-6')}
                source={require('../assets/images/tick.png')}
              />
            </>
          ) : (
            <></>
          )}
        </View>
      </View>
      <Text style={[tailwind('text-sm'), styles.RegularFont, styles.ColorGray]}>
        {props.location}
      </Text>
      <Text
        style={[
          tailwind('text-sm'),
          mystyles.border,
          styles.RegularFont,
          styles.ColorGray,
        ]}>
        {props.amount}
      </Text>
      {props.items.map((item) => {
        return (
          <Text
            key={item}
            style={[
              tailwind('text-sm'),
              styles.RegularFont,
              styles.ColorBlack,
            ]}>
            {item}
          </Text>
        );
      })}

      <Text style={[tailwind('text-sm'), styles.RegularFont, styles.ColorGray]}>
        {props.date}
      </Text>
      <View
        style={tailwind(
          'flex flex-row justify-between mt-4 mb-2 items-center',
        )}>
        <Pressable
          style={tailwind('border-2 border-red-500 rounded-xl px-6 py-3')}>
          <Text
            style={[
              tailwind('text-base uppercase'),
              styles.SemiBoldFont,
              styles.ColorPrimary,
            ]}>
            Reorder
          </Text>
        </Pressable>
        <Pressable
          style={tailwind('border-2 border-red-500 rounded-xl px-6 py-3')}>
          <Text
            style={[
              tailwind('text-base uppercase'),
              styles.SemiBoldFont,
              styles.ColorPrimary,
            ]}>
            Rate Order
          </Text>
        </Pressable>
      </View>
      <Text
        style={[
          tailwind('text-sm py-2 uppercase'),
          styles.RegularFont,
          styles.ColorPrimary,
        ]}>
        View More Orders
      </Text>
    </View>
  );
}

const mystyles = StyleSheet.create({
  border: {
    borderWidth: 1,
    borderStyle: 'dashed',
    borderRadius: 1,
    borderTopColor: 'transparent',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    paddingBottom: 10,
    borderBottomColor: '#6D6E70',
  },
});
