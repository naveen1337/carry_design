import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function cartactions(props) {
  return (
    <Pressable
      elevation={3}
      style={tailwind(
        'm-3 p-3 rounded-lg items-center bg-gray-100 flex flex-row justify-between ',
      )}>
      <Text style={[tailwind('text-base text-gray-500'), styles.SemiBoldFont]}>
        {props.text}
      </Text>
      <Image
        resizeMode={'contain'}
        style={tailwind('w-7 h-7')}
        source={require('../assets/images/pinkplus.png')}
      />
    </Pressable>
  );
}
