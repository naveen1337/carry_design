import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function UserAddress(props) {
  return (
    <View style={tailwind('py-4 border-b-2 border-gray-300')}>
      <View style={tailwind('flex flex-row px-4')}>
        {props.type === 'home' ? (
          <Image
            resizeMode={'contain'}
            style={tailwind('w-6 h-6')}
            source={require('../assets/images/homeaddress.png')}
          />
        ) : (
          <></>
        )}
        {props.type === 'work' ? (
          <Image
            resizeMode={'contain'}
            style={tailwind('w-6 h-6')}
            source={require('../assets/images/work_address.png')}
          />
        ) : (
          <></>
        )}
        {props.type === 'other' ? (
          <Image
            resizeMode={'contain'}
            style={tailwind('w-6 h-6')}
            source={require('../assets/images/location.png')}
          />
        ) : (
          <></>
        )}
        <View style={tailwind('px-4')}>
          <Text
            style={[
              tailwind('uppercase text-sm'),
              styles.ColorBlack,
              styles.SemiBoldFont,
            ]}>
            {props.type}
          </Text>
          <Text
            style={[
              tailwind('text-sm pt-2'),
              styles.ColorBlack,
              styles.RegularFont,
            ]}>
            {props.address1}
          </Text>
          <Text
            style={[
              tailwind('text-sm'),
              styles.ColorBlack,
              styles.RegularFont,
            ]}>
            {props.address2}
          </Text>
          <Text
            style={[
              tailwind('text-sm'),
              styles.ColorBlack,
              styles.RegularFont,
            ]}>
            {props.address3}
          </Text>
          <View style={tailwind('flex flex-row py-1')}>
            <Pressable onPress={()=>alert("1")}>
              <Text
                style={[
                  tailwind('uppercase text-base py-1'),
                  styles.SemiBoldFont,
                  styles.ColorPrimary,
                ]}>
                Edit
              </Text>
            </Pressable>
            <Pressable>
              <Text
                style={[
                  tailwind('uppercase text-base px-6 py-1'),
                  styles.SemiBoldFont,
                  styles.ColorPrimary,
                ]}>
                Delete
              </Text>
            </Pressable>
          </View>
        </View>
      </View>
    </View>
  );
}
