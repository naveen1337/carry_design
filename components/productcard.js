import React, {useState, useEffect} from 'react';
import {View, Text, Image, Pressable, ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import Modal from 'react-native-modal';
import {Picker} from '@react-native-picker/picker';

export default function productcard(props) {
  let [quantity, setquantity] = useState(0);
  let [svar, setsvar] = useState([]);
  let [addons, setaddons] = useState([]);
  const [addonmodal, setaddonmodal] = useState(false);

  // set variation quantity as 0
  useEffect(() => {
    if (props.hasvariation) {
      setsvar(props.variation[0]);
    } else {
      // Pass now
    }
    if (props.hasaddon) {
      props.addons.forEach((item) => {
        item.checked = false;
      });
      setaddons(props.addons);
    }
  }, []);
  function increase(e) {
    setquantity(quantity + 1);
  }
  function decrease(e) {
    if (quantity > 0) {
      setquantity(quantity - 1);
    } else {
      return false;
    }
  }
  return (
    <View style={[tailwind('flex flex-row justify-between mx-4 p-2')]}>
      <View style={tailwind('w-8/12')}>
        {props.bestseller ? (
          <View style={tailwind('flex flex-row items-center py-1')}>
            <Image
              resizeMode={'contain'}
              style={tailwind('w-3 h-3')}
              source={require('../assets/images/star.png')}
            />
            <Text style={[tailwind('text-xs px-2'), styles.RegularFont]}>
              Best Seller
            </Text>
          </View>
        ) : (
          <View style={tailwind('flex flex-row items-center py-1')}></View>
        )}

        <View style={tailwind('flex flex-row items-center')}>
          {props.veg ? (
            <Image
              style={tailwind('w-3 h-3')}
              source={require('../assets/images/veg.png')}
            />
          ) : (
            <Image
              style={tailwind('w-3 h-3')}
              source={require('../assets/images/nonveg.png')}
            />
          )}
          <Text
            numberOfLines={2}
            style={[
              styles.SemiBoldFont,
              styles.ColorBlack,
              tailwind('text-base px-1'),
            ]}>
            {props.name}
          </Text>
        </View>

        {props.hasdesc ? (
          <Text
            style={[
              tailwind('text-xs py-2'),
              styles.RegularFont,
              styles.ColorGray,
            ]}>
            {props.desc}
          </Text>
        ) : null}

        {props.hasvariation ? (
          <View style={tailwind('flex flex-row items-center py-1')}>
            <View
              style={[
                tailwind('border rounded border-gray-300'),
                {width: 100},
              ]}>
              <Picker
                style={{width: '100%', height: 30}}
                mode={'dropdown'}
                onValueChange={(itemValue, itemIndex) => {
                  console.log(itemValue);
                }}>
                <Picker.Item label="One" value="One" />
                <Picker.Item label="Two" value="Two" />
                <Picker.Item label="Three" value="Three" />
              </Picker>
            </View>

            <View style={tailwind('flex flex-row items-center px-2')}>
              <Text style={[tailwind('text-base'), styles.SemiBoldFont]}>
                ₹ {svar.amount}
              </Text>
              <Text
                style={[
                  tailwind('text-xs px-1 text-gray-500 line-through'),
                  styles.SemiBoldFont,
                ]}>
                ₹ {svar.demo_amount}
              </Text>
            </View>
          </View>
        ) : (
          <View
            style={tailwind(
              `flex flex-row items-center  ${props.hasdesc ? '' : 'py-2 px-2'}`,
            )}>
            <Text style={[tailwind('text-base'), styles.SemiBoldFont]}>
              ₹ 100
            </Text>
            <Text
              style={[
                tailwind('text-xs px-1 text-gray-500 line-through'),
                styles.SemiBoldFont,
              ]}>
              ₹ 150
            </Text>
          </View>
        )}
      </View>

      {/* Product Images */}
      <View style={tailwind('w-4/12 mt-3')}>
        {props.hasimage ? (
          <View style={tailwind('')}>
            <Image
              resizeMode={'contain'}
              style={[tailwind('rounded-lg'), {width: 110, height: 100}]}
              source={{uri: props.image}}
            />
          </View>
        ) : null}

        {/* <View
          style={[
            tailwind(
              `${props.hasimage ? 'relative w-24 bottom-4 mx-2' : 'h-full'}`,
            ),
          ]}> */}

        <View
          style={[
            props.hasimage
              ? {
                  position: 'relative',
                  bottom: 20,
                  marginLeft: 15,
                  marginRight: 10,
                }
              : {height: '100%'},
          ]}>
          {quantity > 0 ? (
            <View
              style={[
                tailwind(
                  'flex flex-row py-1 justify-between items-center rounded-lg',
                ),
                styles.BackgroundPrimary,
              ]}>
              <Pressable onPress={() => decrease()} style={tailwind('')}>
                <Icon name="remove" size={21} color="#ffffff" />
              </Pressable>
              <Text style={[styles.BoldFont, tailwind(' text-white')]}>
                {quantity}
              </Text>
              <Pressable
                onPress={() => increase()}
                style={[tailwind(''), styles.BoldFont]}>
                <Icon name="add" size={22} color="#ffffff" />
              </Pressable>
            </View>
          ) : (
            <Pressable
              onPress={() => increase()}
              style={[tailwind('rounded-lg py-2'), styles.BackgroundPrimary]}>
              <Text
                style={[
                  tailwind('text-white text-xs text-center uppercase'),
                  styles.RegularFont,
                ]}>
                Add
              </Text>
            </Pressable>
          )}
        </View>
      </View>

      {/* Add on Modal */}
      <Modal
        style={{justifyContent: 'flex-end', margin: 0}}
        isVisible={addonmodal}>
        <View style={tailwind('bg-white pt-2 h-96')}>
          <Text style={[tailwind('py-2 px-3 text-xl'), styles.BoldFont]}>
            The Example Text
          </Text>
          <Text style={[tailwind('py-1 px-3 text-base'), styles.RegularFont]}>
            Add ons (4)
          </Text>
          <ScrollView>
            {addons.map((item) => {
              return (
                <Pressable
                  key={item.id}
                  style={tailwind(
                    'flex flex-row justify-between items-center py-3 px-2 border border-gray-300 rounded-lg my-2 mx-2',
                  )}>
                  <View style={tailwind('flex flex-row items-center')}>
                    {!item.checked ? (
                      <Icon name="checkbox-outline" size={25} color="green" />
                    ) : (
                      <Icon name="square-outline" size={25} color="gray" />
                    )}

                    <Text
                      style={[
                        tailwind('text-base px-3 text-black'),
                        styles.SemiBoldFont,
                      ]}>
                      {item.name}
                    </Text>
                  </View>

                  <Text
                    style={[tailwind('text-base px-3'), styles.SemiBoldFont]}>
                    {' '}
                    ₹ {item.price}
                  </Text>
                </Pressable>
              );
            })}
          </ScrollView>
          <View style={tailwind('flex flex-row justify-between my-3 mx-3')}>
            <Pressable
              onPress={() => setaddonmodal(false)}
              style={[
                tailwind('py-3 w-6/12 flex-grow'),
                styles.BackgroundPurpleLight,
              ]}>
              <Text
                style={[
                  tailwind('text-center text-white text-base uppercase'),
                  styles.SemiBoldFont,
                ]}>
                Close
              </Text>
            </Pressable>
            <Pressable
              style={[tailwind('py-3 w-6/12'), styles.BackgroundPurpleDark]}>
              <Text
                style={[
                  tailwind('text-center text-white text-base uppercase'),
                  styles.SemiBoldFont,
                ]}>
                Add Item
              </Text>
            </Pressable>
          </View>
        </View>
      </Modal>
    </View>
  );
}
