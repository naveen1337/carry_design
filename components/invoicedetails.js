import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function InvoiceDetails() {
  return (
    <View style={tailwind('')}>
      <Text
        style={[
          tailwind('uppercase text-base px-4 pt-3 pb-8'),
          styles.RegularFont,
          styles.ColorBlack,
        ]}>
        Invoice Details
      </Text>
      <View style={tailwind('border border-gray-300 rounded-lg mx-4')}>
        <View style={tailwind('flex flex-row justify-between px-4 mt-4 ')}>
          <Text
            style={[
              tailwind('text-sm w-9/12'),
              styles.RegularFont,
              styles.ColorGray,
            ]}>
            Item(s) total
          </Text>
          <Text
            style={[tailwind('text-sm'), styles.RegularFont, styles.ColorGray]}>
            ₹ 500
          </Text>
        </View>

        <View style={tailwind('flex flex-row justify-between my-1 px-4 ')}>
          <Text
            style={[
              tailwind('text-sm w-9/12'),
              styles.RegularFont,
              styles.ColorPrimary,
            ]}>
            Delivery Partner Fee
          </Text>
          <Text
            style={[tailwind('text-sm'), styles.RegularFont, styles.ColorGray]}>
            ₹ 50
          </Text>
        </View>
        <Text
          style={[
            tailwind('text-gray-500 mx-4'),
            styles.RegularFont,
            styles.px8,
          ]}>
          This fee goes towards your Delivery Partner
        </Text>
        <View style={tailwind('flex flex-row justify-between px-4 pt-2 ')}>
          <Text
            style={[
              tailwind('text-sm w-9/12'),
              styles.RegularFont,
              styles.ColorGray,
            ]}>
            Packing Fee
          </Text>
          <Text style={[tailwind('text-sm text-gray-600'), styles.RegularFont]}>
            ₹ 10
          </Text>
        </View>
        <View
          style={tailwind(
            'flex flex-row px-4 justify-between mt-1 mx-3 pb-3 border-b-2 border-gray-800  ',
          )}></View>

        <View style={tailwind('flex flex-row justify-between px-4 mt-4 ')}>
          <Text
            style={[
              tailwind('text-sm w-9/12'),
              styles.RegularFont,
              styles.ColorGray,
            ]}>
            Delivery Tip
          </Text>
          <Text style={[tailwind('text-sm text-red-600'), styles.RegularFont]}>
            Add a Tip
          </Text>
        </View>

        <View style={tailwind('flex flex-row justify-between my-1 px-4 ')}>
          <Text
            style={[
              tailwind('text-sm w-9/12 text-red-600'),
              styles.RegularFont,
            ]}>
            Taxes and Charges
          </Text>
          <Text
            style={[tailwind('text-sm'), styles.RegularFont, styles.ColorGray]}>
            ₹ 50
          </Text>
        </View>

        <View style={tailwind('flex flex-row justify-between my-1 px-4 pb-4 ')}>
          <Text
            style={[
              tailwind('text-sm w-9/12'),
              styles.RegularFont,
              styles.ColorBlack,
            ]}>
            To Pay
          </Text>
          <Text
            style={[tailwind('text-sm'), styles.RegularFont, styles.ColorGray]}>
            ₹ 500
          </Text>
        </View>
      </View>
    </View>
  );
}
