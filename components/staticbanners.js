import React from 'react';
import {View, Text, Image, Pressable, Dimensions} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function Tip() {
  return (
    <View>
      <View style={tailwind('flex flex-row justify-center')}>
        <Image
          resizeMethod={'resize'}
          resizeMode={'contain'}
          style={{
            height: 110,
            width: Dimensions.get('window').width - 25,
          }}
          source={require('../assets/images/sbanner1.png')}
        />
      </View>

      <View style={tailwind('flex flex-row my-3 justify-center')}>
        <View>
          <Image
            style={{
              height: 110,
              width: Dimensions.get('window').width / 2,
            }}
            resizeMode={'contain'}
            source={require('../assets/images/sbanner2.png')}
          />
        </View>
        <View>
          <Image
            style={{
              height: 110,
              width: Dimensions.get('window').width / 2,
            }}
            resizeMode={'contain'}
            source={require('../assets/images/sbanner3.png')}
          />
        </View>
      </View>
    </View>
  );
}
