import React from 'react';
import {Image, Pressable, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import Collapsible from 'react-native-collapsible';
import {styles} from '../constants/styles';

export default function UserProfile(props) {
  let [status, setstatus] = React.useState(false);
  return (
    <View style={tailwind('')}>
      <Pressable
        onPress={() => setstatus(!status)}
        style={tailwind(
          'mx-2 pt-4 pb-2  flex flex-row justify-between items-center',
        )}>
        <Text
          style={[
            tailwind('text-base'),
            styles.SemiBoldFont,
            styles.ColorBlack,
          ]}>
          {props.header}
        </Text>
        {status ? (
          <Icon name="chevron-down-outline" size={25} color="#FF2E2E" />
        ) : (
          <Icon name="chevron-up-outline" size={25} color="#FF2E2E" />
        )}
      </Pressable>

      {props.variant === 'account' ? (
        <Collapsible
          style={tailwind('border-t-2 border-gray-400')}
          collapsed={status}>
          <Pressable
            style={tailwind(
              ' mx-4 flex py-2 my-1 flex-row items-center justify-between',
            )}>
            <View style={tailwind('flex flex-row items-center')}>
              <Image
                resizeMode={'contain'}
                style={tailwind('w-6 h-6')}
                source={require('../assets/images/manage_address.png')}
              />
              <Text
                style={[
                  tailwind('px-3'),
                  styles.SemiBoldFont,
                  styles.ColorBlack,
                ]}>
                Manage Address
              </Text>
            </View>
            <Icon name="chevron-forward-outline" size={20} color="#FF2E2E" />
          </Pressable>

          <Pressable
            style={tailwind(' flex py-2 mx-4 my-1 flex-row justify-between')}>
            <View style={tailwind('flex flex-row')}>
              <Icon name="heart-outline" size={20} color="#FF2E2E" />
              <Text
                style={[
                  tailwind('px-3'),
                  styles.SemiBoldFont,
                  styles.ColorBlack,
                ]}>
                Favorites
              </Text>
            </View>
            <Icon name="chevron-forward-outline" size={20} color="#FF2E2E" />
          </Pressable>

          <Pressable
            style={tailwind(' flex py-2 mx-4 my-1 flex-row justify-between')}>
            <View style={tailwind('flex flex-row')}>
              <Icon
                name="notifications-circle-outline"
                size={25}
                color="#FF2E2E"
              />
              <Text
                style={[
                  tailwind('px-3'),
                  styles.SemiBoldFont,
                  styles.ColorBlack,
                ]}>
                Notification
              </Text>
            </View>
            <Icon name="chevron-forward-outline" size={20} color="#FF2E2E" />
          </Pressable>

          <Pressable
            style={tailwind(' flex py-2 mx-4 my-2 flex-row justify-between')}>
            <View style={tailwind('flex flex-row')}>
              <Image
                resizeMode={'contain'}
                style={tailwind('w-5 h-5')}
                source={require('../assets/images/offers.png')}
              />
              <Text
                style={[
                  tailwind('px-3'),
                  styles.SemiBoldFont,
                  styles.ColorBlack,
                ]}>
                Offers
              </Text>
            </View>
            <Icon name="chevron-forward-outline" size={20} color="#FF2E2E" />
          </Pressable>
        </Collapsible>
      ) : (
        <></>
      )}

      {props.variant === 'payment' ? (
        <Collapsible
          style={tailwind('border-t-2 border-gray-400')}
          collapsed={status}>
          <Pressable
            style={tailwind(' mx-4 flex py-2 my-2 flex-row justify-between')}>
            <View style={tailwind('flex flex-row')}>
              <Image
                resizeMode={'contain'}
                style={tailwind('w-5 h-5')}
                source={require('../assets/images/refund_status.png')}
              />
              <Text
                style={[
                  tailwind('px-3'),
                  styles.SemiBoldFont,
                  styles.ColorBlack,
                ]}>
                Refund Status
              </Text>
            </View>
            <Icon name="chevron-forward-outline" size={20} color="#FF2E2E" />
          </Pressable>

          <Pressable
            style={tailwind(' mx-4 flex py-2 my-1 flex-row justify-between')}>
            <View style={tailwind('flex flex-row')}>
              <Image
                resizeMode={'contain'}
                style={tailwind('w-5 h-5')}
                source={require('../assets/images/pay_mode.png')}
              />
              <Text
                style={[
                  tailwind('px-3'),
                  styles.SemiBoldFont,
                  styles.ColorBlack,
                ]}>
                Payment Modes
              </Text>
            </View>
            <Icon name="chevron-forward-outline" size={20} color="#FF2E2E" />
          </Pressable>
        </Collapsible>
      ) : (
        <></>
      )}

      {props.variant === 'faq' ? (
        <Collapsible
          style={tailwind('border-t-2 border-gray-400')}
          collapsed={status}>
          <Pressable
            style={tailwind(' mx-4 flex py-2 my-1 flex-row justify-between')}>
            <View style={tailwind('flex flex-row')}>
              <Image
                resizeMode={'contain'}
                style={tailwind('w-5 h-5')}
                source={require('../assets/images/pay_mode.png')}
              />
              <Text
                style={[
                  tailwind('px-3'),
                  styles.SemiBoldFont,
                  styles.ColorBlack,
                ]}>
                General Issues
              </Text>
            </View>
            <Icon name="chevron-forward-outline" size={20} color="#FF2E2E" />
          </Pressable>

          <Pressable
            style={tailwind(' mx-4 flex py-2 my-1 flex-row justify-between')}>
            <View style={tailwind('flex flex-row')}>
              <Image
                resizeMode={'contain'}
                style={tailwind('w-5 h-5')}
                source={require('../assets/images/issues.png')}
              />
              <Text
                style={[
                  tailwind('px-3'),
                  styles.SemiBoldFont,
                  styles.ColorBlack,
                ]}>
                Legal, Terms & Conditions
              </Text>
            </View>
            <Icon name="chevron-forward-outline" size={20} color="#FF2E2E" />
          </Pressable>

          <Pressable
            style={tailwind(' mx-4 flex py-2 my-1 flex-row justify-between')}>
            <View style={tailwind('flex flex-row')}>
              <Image
                resizeMode={'contain'}
                style={tailwind('w-5 h-5')}
                source={require('../assets/images/faq.png')}
              />
              <Text
                style={[
                  tailwind('px-3'),
                  styles.SemiBoldFont,
                  styles.ColorBlack,
                ]}>
                Faq's
              </Text>
            </View>
            <Icon name="chevron-forward-outline" size={20} color="#FF2E2E" />
          </Pressable>

          <Pressable
            style={tailwind(' mx-4 flex py-2 my-1 flex-row justify-between')}>
            <View style={tailwind('flex flex-row')}>
              <Image
                resizeMode={'contain'}
                style={tailwind('w-5 h-5')}
                source={require('../assets/images/chat.png')}
              />
              <Text
                style={[
                  tailwind('px-3'),
                  styles.SemiBoldFont,
                  styles.ColorBlack,
                ]}>
                Chat Support
              </Text>
            </View>
            <Icon name="chevron-forward-outline" size={20} color="#FF2E2E" />
          </Pressable>

          <Pressable
            style={tailwind(' mx-4 flex py-2 my-1 flex-row justify-between')}>
            <View style={tailwind('flex flex-row')}>
              <Image
                resizeMode={'contain'}
                style={tailwind('w-5 h-5')}
                source={require('../assets/images/referral.png')}
              />
              <Text
                style={[
                  tailwind('px-3'),
                  styles.SemiBoldFont,
                  styles.ColorBlack,
                ]}>
                Referral
              </Text>
            </View>
            <Icon name="chevron-forward-outline" size={20} color="#FF2E2E" />
          </Pressable>
        </Collapsible>
      ) : (
        <></>
      )}
    </View>
  );
}
