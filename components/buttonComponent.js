import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function ButtonComponent(props) {
  return (
    <View style={[tailwind('py-4 rounded-lg'), styles.BackgroundPrimary]}>
      <Text style={[tailwind('text-white text-center'), styles.SemiBoldFont]}>
        {props.text}
      </Text>
    </View>
  );
}
