import React from 'react';
import {View, Text, TextInput, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function DeliveryTextInput(props) {
  return (
    <View
      elevation={3}
      style={tailwind(
        'm-3 px-4 rounded-lg items-center bg-gray-100 flex flex-row justify-between ',
      )}>
      <TextInput
        placeholder="Enter Delivery Instructions"
        placeholderTextColor="#707070"
        style={[
          tailwind('text-base flex-grow'),
          styles.SemiBoldFont,
          styles.ColorBlack,
        ]}
      />
    </View>
  );
}
