import React from 'react';
import {View, Text, Image} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function restaurantoffers(props) {
  return (
    <View style={tailwind('border p-3 border-gray-900 ml-4 w-48 rounded')}>
      <View style={tailwind('flex flex-row items-center')}>
        <Image
          source={require('../assets/images/offer.png')}
          style={tailwind('w-5 h-5')}
          resizeMode={'contain'}
        />
        <Text
          numberOfLines={1}
          style={[
            tailwind('text-base px-1'),
            styles.SemiBoldFont,
            styles.ColorGray,
          ]}>
          {props.offer}
        </Text>
      </View>
      <Text
        style={[tailwind('text-xs text-gray-500 pt-1'), styles.RegularFont]}>
        {props.offercode}
      </Text>
    </View>
  );
}
