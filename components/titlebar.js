import React from 'react';
import {View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function titlebar(props) {
  if (props.varient === 'textonly')
    return (
      <View style={tailwind('py-2 flex flex-row justify-between')}>
        <Text style={[tailwind('text-lg px-4'), styles.SemiBoldFont]}>
          {props.title}
        </Text>
        <Text style={[tailwind('text-lg px-4'), styles.SemiBoldFont]}>
          {props.subtitle}
        </Text>
      </View>
    );
}
