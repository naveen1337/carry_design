import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function HomeCoupons(props) {
  return (
    <View style={tailwind('')}>
      {props.image === 1 ? (
        <Image
          resizeMode={'contain'}
          style={tailwind('w-32 h-32  ')}
          source={require('../assets/images/offer1.png')}
        />
      ) : (
        <></>
      )}
      {props.image === 2 ? (
        <Image
          resizeMode={'contain'}
          style={tailwind('w-32 h-32  ')}
          source={require('../assets/images/offer2.png')}
        />
      ) : (
        <></>
      )}
      {props.image === 3 ? (
        <Image
          resizeMode={'contain'}
          style={tailwind('w-32 h-32  ')}
          source={require('../assets/images/offer3.png')}
        />
      ) : (
        <></>
      )}
    </View>
  );
}
