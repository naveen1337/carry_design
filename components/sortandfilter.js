import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function SortandFilter() {
  return (
    <View
      style={tailwind('mx-6 py-4 flex flex-row justify-between items-center')}>
      <View style={tailwind('flex flex-row items-center')}>
        <Icon name="options-outline" size={20} color="#FF2E2E" />
        <Text
          style={[
            tailwind('uppercase text-sm px-3'),
            styles.RegularFont,
            styles.ColorGray,
          ]}>
          Sort / Filter
        </Text>
      </View>
      <View style={tailwind('flex flex-row items-center')}>
        <Text
          style={[
            tailwind('uppercase text-sm px-3'),
            styles.RegularFont,
            styles.ColorGray,
          ]}>
          View all
        </Text>
        <Image
          style={tailwind('w-4 h-4')}
          source={require('../assets/icons/arrow_right.png')}
        />
      </View>
    </View>
  );
}
