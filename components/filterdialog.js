import React from 'react';
import {
  View,
  Text,
  Image,
  Pressable,
  StyleSheet,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import Modal from 'react-native-modal';
import CheckBox from '@react-native-community/checkbox';

export default function Filter(props) {
  let [relevence, setrelevence] = React.useState(false);
  let [costfortwo, setcostfortwo] = React.useState(false);
  let [deliverytime, setdeliverytime] = React.useState(false);
  let [rating, setrating] = React.useState(false);
  return (
    <Modal isVisible={props.status}>
      <View style={[tailwind('bg-gray-200 p-3')]}>
        <View style={[tailwind('flex flex-row justify-between items-center')]}>
          <View style={tailwind('flex flex-row items-center')}>
            <Pressable onPress={() => props.closemodal()}>
              <Icon name="close-outline" size={30} color="#FF2E2E" />
            </Pressable>

            <Text
              style={[
                tailwind('uppercase text-sm px-3'),
                styles.SemiBoldFont,
                styles.ColorBlack,
              ]}>
              Sort and filter
            </Text>
          </View>
          <Text
            style={[
              tailwind('text-base uppercase'),
              styles.ColorPrimary,
              styles.SemiBoldFont,
            ]}>
            Clear all
          </Text>
        </View>
        <Text
          style={[
            styles.BoldFont,
            styles.ColorBlack,
            tailwind('py-1 text-base px-3'),
          ]}>
          Sort
        </Text>

        <Text
          style={[
            styles.RegularFont,
            styles.ColorGray,
            tailwind('text-xs px-3 py-1'),
          ]}>
          Show Restaurants by
        </Text>

        <View style={tailwind('px-3 py-1 flex flex-row justify-between')}>
          <View style={tailwind('w-6/12  flex flex-row items-center')}>
            <Pressable onPress={() => setrelevence(!relevence)}>
              {relevence ? (
                <Icon name="radio-button-off-outline" size={18} color="red" />
              ) : (
                <Icon name="radio-button-on-outline" size={18} color="red" />
              )}
            </Pressable>
            <Text
              style={[
                tailwind('text-base px-2'),
                styles.SemiBoldFont,
                styles.ColorBlack,
              ]}>
              Relevance
            </Text>
          </View>
          <View style={tailwind('w-6/12  flex flex-row items-center')}>
            <Pressable onPress={() => setcostfortwo(!costfortwo)}>
              {costfortwo ? (
                <Icon name="radio-button-off-outline" size={18} color="red" />
              ) : (
                <Icon name="radio-button-on-outline" size={18} color="red" />
              )}
            </Pressable>

            <Text
              style={[
                tailwind('text-base px-2'),
                styles.SemiBoldFont,
                styles.ColorBlack,
              ]}>
              Cost for Two
            </Text>
          </View>
        </View>

        <View style={tailwind('px-3 py-1 flex flex-row justify-between')}>
          <View style={tailwind('w-6/12  flex flex-row items-center')}>
            <Pressable onPress={() => setdeliverytime(!deliverytime)}>
              {deliverytime ? (
                <Icon name="radio-button-off-outline" size={18} color="red" />
              ) : (
                <Icon name="radio-button-on-outline" size={18} color="red" />
              )}
            </Pressable>
            <Text
              style={[
                tailwind('text-base px-2'),
                styles.SemiBoldFont,
                styles.ColorBlack,
              ]}>
              Delivery Time
            </Text>
          </View>
          <View style={tailwind('w-6/12 flex flex-row items-center')}>
            <Pressable onPress={() => setrating(!rating)}>
              {rating ? (
                <Icon name="radio-button-off-outline" size={18} color="red" />
              ) : (
                <Icon name="radio-button-on-outline" size={18} color="red" />
              )}
            </Pressable>
            <Text
              style={[
                tailwind('text-base px-2'),
                styles.SemiBoldFont,
                styles.ColorBlack,
              ]}>
              Rating
            </Text>
          </View>
        </View>

        {/* Cuisines Start */}
        <Text
          style={[
            styles.BoldFont,
            styles.ColorBlack,
            tailwind('py-1 text-base px-3'),
          ]}>
          Cuisines
        </Text>

        <View style={tailwind('flex flex-row flex-wrap')}>
          {[
            'Asian',
            'Afgani',
            'Arabian',
            'Chinese',
            'Chat',
            'Cafe',
            'Grill',
          ].map((item) => {
            return (
              <Text
                key={item}
                style={[tailwind(
                  'border rounded-xl mx-1 my-2  px-4 py-2 border-gray-500',
                ),styles.RegularFont]}>
                {item}
              </Text>
            );
          })}
        </View>

        {/* Offer & More */}

        <Text
          style={[
            styles.BoldFont,
            styles.ColorBlack,
            tailwind('py-1 text-base px-3'),
          ]}>
          Offers & More
        </Text>

        <Text
          style={[
            styles.RegularFont,
            styles.ColorGray,
            tailwind('text-xs px-3 py-1'),
          ]}>
          Show Restaurants with
        </Text>
        <View style={tailwind('flex flex-row flex-wrap')}>
          {['Asian', 'Afgani', 'Arabian'].map((item) => {
            return (
              <Text
                key={item}
                style={[tailwind(
                  'border rounded-xl mx-1 my-2  px-4 py-2 border-gray-500',
                ),styles.RegularFont]}>
                {item}
              </Text>
            );
          })}
        </View>
        {/* End of Offers & More */}
        <Pressable
          onPress={() => props.closemodal()}
          style={[tailwind('rounded-lg py-4 my-3'), styles.BackgroundPrimary]}>
          <Text
            style={[
              tailwind('uppercase text-center text-white'),
              styles.SemiBoldFont,
            ]}>
            Apply
          </Text>
        </Pressable>
      </View>
    </Modal>
  );
}

const mystyles = StyleSheet.create({
  modelheight: {
    height: Dimensions.get('window').height / 2,
  },
});
