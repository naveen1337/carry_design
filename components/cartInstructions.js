import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function CartInstruction() {
  return (
    <View style={tailwind('mx-3')}>
      <Text style={[tailwind('text-sm py-5 text-red-600'), styles.RegularFont]}>
        Instructions
      </Text>
      <View style={[tailwind('flex flex-row px-3 my-3')]}>
        <Image
          resizeMode={'contain'}
          source={require('../assets/images/deliveryboy.png')}
          style={tailwind('w-10 h-10')}
        />
        <Text style={[tailwind('w-11/12 px-3'), styles.RegularFont]}>
          If you choose to cancel you can do it within 60 seconds after placing
          the order
        </Text>
      </View>

      <View style={[tailwind('flex flex-row px-3 my-3')]}>
        <Image
          resizeMode={'contain'}
          source={require('../assets/images/deliveryboy.png')}
          style={tailwind('w-10 h-10')}
        />
        <Text style={[tailwind('w-11/12 px-3'), styles.RegularFont]}>
          If you choose to cancel you can do it within 60 seconds after placing
          the order
        </Text>
      </View>

      <View style={[tailwind('flex flex-row px-3 my-3')]}>
        <Image
          resizeMode={'contain'}
          source={require('../assets/images/deliveryboy.png')}
          style={tailwind('w-10 h-10')}
        />
        <Text style={[tailwind('w-11/12 px-3'), styles.RegularFont]}>
          If you choose to cancel you can do it within 60 seconds after placing
          the order
        </Text>
      </View>

      <Text style={[tailwind('text-red-600'),styles.RegularFont]}>Refund Policy</Text>

      
    </View>
  );
}
