import React from 'react';
import {View, Text, Image, Pressable,ScrollView, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function OrderStatusCard() {
  return (
    <View
      style={[
        tailwind('rounded rounded-lg mx-4 p-4'),
        styles.BackgroundPrimary,
      ]}>
      <View style={tailwind('flex flex-row justify-between')}>
        <View>
          <Text
            style={[
              tailwind('text-sm text-white'),
              styles.RegularFont,
              styles.px14,
            ]}>
            Delivery Time
          </Text>
          <View style={tailwind('flex flex-row py-2')}>
            <Icon name="alarm-outline" size={20} color="#ffffff" />
            <Text
              style={[
                tailwind('text-white px-1'),
                styles.px17,
                styles.BoldFont,
              ]}>
              20 Min
            </Text>
          </View>
        </View>

        <View style={tailwind('flex flex-row items-center')}>
          <Image
            resizeMode={'contain'}
            source={require('../assets/images/pick2.png')}
            style={tailwind('w-10 h-10')}
          />
          <Text style={[styles.SemiBoldFont, tailwind('text-white')]}>
            View Order Details
          </Text>
        </View>
      </View>

      <View style={tailwind('flex flex-row items-center py-2')}>
        <View style={tailwind('w-20 h-20 bg-white rounded')}></View>
        <View style={tailwind('px-4')}>
          <Text style={[tailwind('text-3xl text-white'), styles.SemiBoldFont]}>
            Kumaran
          </Text>
          <Text
            style={[tailwind('text-white'), styles.SemiBoldFont, styles.px14]}>
            Delivery Person
          </Text>
        </View>
        <View
          style={[tailwind('p-3 rounded rounded-lg'), mystyles.callbackground]}>
          <Icon name="call" color={'#ffffff'} size={25} />
        </View>
      </View>

      <View style={tailwind('flex flex-row py-3 items-center')}>
        <View style={tailwind('bg-white rounded-lg p-1')}>
          <Image
            resizeMode={'contain'}
            style={tailwind('w-10 h-10 bg-white')}
            source={require('../assets/images/issues.png')}
          />
        </View>
        <View style={tailwind('flex-grow pb-2 pl-4')}>
          <Text
            style={[tailwind('text-white'), styles.SemiBoldFont, styles.px14]}>
            Order Confirmed
          </Text>
          <Text
            style={[
              tailwind('text-white border-b-2 pb-3 border-white'),
              styles.SemiBoldFont,
              styles.px14,
            ]}>
            Your order has been Confirmed
          </Text>
        </View>
      </View>

      <View style={tailwind('flex flex-row py-3 items-center')}>
        <View style={tailwind('bg-white rounded-lg p-1')}>
          <Image
            resizeMode={'contain'}
            style={tailwind('w-10 h-10 bg-white')}
            source={require('../assets/images/pick.png')}
          />
        </View>
        <View style={tailwind('flex-grow pb-2 pl-4')}>
          <Text
            style={[tailwind('text-white'), styles.SemiBoldFont, styles.px14]}>
            Order Prepared
          </Text>
          <Text
            style={[
              tailwind('text-white border-b-2 pb-3 border-white'),
              styles.SemiBoldFont,
              styles.px14,
            ]}>
            Your order has been Prepared
          </Text>
        </View>
      </View>

      <View style={tailwind('flex flex-row py-3 items-center')}>
        <View style={tailwind('bg-white rounded-lg p-1')}>
          <Image
            resizeMode={'contain'}
            style={tailwind('w-10 h-10 bg-white')}
            source={require('../assets/images/cart.png')}
          />
        </View>
        <View style={tailwind('flex-grow pb-2 pl-4')}>
          <Text
            style={[tailwind('text-white'), styles.SemiBoldFont, styles.px14]}>
            Delivery in progress
          </Text>
          <Text
            style={[
              tailwind('text-white border-b-2 pb-3 border-white'),
              styles.SemiBoldFont,
              styles.px14,
            ]}>
            Hang on! Your food is on the way
          </Text>
        </View>
      </View>
    </View>
  );
}

const mystyles = StyleSheet.create({
  callbackground: {
    backgroundColor: '#8A1A4D',
  },
});
