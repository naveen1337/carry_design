import React from 'react';
import {
  View,
  Text,
  Image,
  Pressable,
  StyleSheet,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import CheckBox from '@react-native-community/checkbox';

export default function Tip() {
  const [toggle, settoggle] = React.useState(false);
  return (
    <View style={tailwind('py-4 mx-3 flex flex-row')}>
      <CheckBox
        value={toggle}
        tintColors={{true: '#FF2E2E', false: '#6A625E'}}
        onValueChange={(newValue) => settoggle(!toggle)}
      />
      <View style={tailwind('px-3')}>
        <Text
          style={[tailwind('text-lg'), styles.RegularFont, styles.ColorBlack]}>
          Opt in for No-contact Delivery
        </Text>
        <Text
          style={[
            tailwind(''),
            customstyles.text,
            styles.RegularFont,
            styles.ColorGray,
          ]}>
          Our Carry delivery person will call you (or ring your doorbell) once
          the order is placed at your door. (Not applicable for COD)
        </Text>
      </View>
    </View>
  );
}

const customstyles = StyleSheet.create({
  text: {
    fontSize: 8,
    color: '#58585B',
    marginVertical: 5,
    width: Dimensions.get('window').width - 100,
  },
});
